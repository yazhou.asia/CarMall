﻿using BrnMall.Services;
using BrnMall.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarBuy.Web.Controllers
{
    public class HomeController : BaseWebController
    {
        public List<BrnMall.Core.CategoryInfo> _listCategoryInfo { get; set; }
        //BrnMall.Data.Categories categories = new BrnMall.Data.Categories();
        // GET: Home
        public ActionResult Index()
        {
            _listCategoryInfo = BrnMall.Data.Categories.GetCategoryList().OrderBy(m=>m.Name).ToList();
            foreach (var item in _listCategoryInfo)
            {
                item._letter = MallUtils.GetSpellCode(item.Name);
            }

            return View();
        }
    }
}