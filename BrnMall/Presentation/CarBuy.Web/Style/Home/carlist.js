﻿
/*
Append File:/2sc/pc/UsedCarList/v6/Command.js
*/

function StringBuffer() {
    this.__strings__ = [];
};
StringBuffer.prototype.Append = function (str) {
    this.__strings__.push(str);
    return this;
};
//格式化字符串
StringBuffer.prototype.AppendFormat = function (str) {
    for (var i = 1; i < arguments.length; i++) {
        var parent = "\\{" + (i - 1) + "\\}";
        var reg = new RegExp(parent, "g")
        str = str.replace(reg, arguments[i]);
    }

    this.__strings__.push(str);
    return this;
}
StringBuffer.prototype.ToString = function () {
    return this.__strings__.join('');
};
StringBuffer.prototype.empty = function () {
    this.__strings__.length = 0;
}
StringBuffer.prototype.size = function () {
    return this.__strings__.length;
}
String.IsNullOrEmpty = function (str) {
    if (typeof (str) !== "string")
        return true;
    if ($.trim(str) == "")
        return true;
    return false;
};
String.Empty = "";
String.Join = function (char, array) {
    return array.join(char);
}
String.Format = function (str) {
    for (var i = 1; i < arguments.length; i++) {
        var parent = "\\{" + (i - 1) + "\\}";
        var reg = new RegExp(parent, "g")
        str = str.replace(reg, arguments[i]);
    }
    return str;
}
String.prototype.getByteLen = function () {
    var len = 0;
    for (var i = 0; i < this.length; i++) {
        if (this.charAt(i).match(/[^x00-xff]/ig) != null) //全角
            len += 2;
        else
            len += 1;
    };
    return len;
}
String.IsNullOrEmpty = function (str) {
    if (typeof (str) !== "string")
        return true;
    if ($.trim(str) == "")
        return true;
    if (str == "null")
        return true;
    return false;
};
var string = String;


var Tool = {};
//数组是否包含元素
Tool.Exist = function (array, item) {
    if (array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == item)
                return true;
        }
    }
    return false;
};
//转换数字型
Tool.GetNumber = function (str) {
    switch (typeof (str)) {
        case "number":
            return str;
        case "undefined":
        case "obj":
        case "function":
            return 0;
        case "boolean":
            return Number(str);
    }

    var reg = /^[0-9]+.?[0-9]*$/;
    if (reg.test(str))
        return Number(str);
    return 0;
};
//是否数字
Tool.IsNumber = function (str) {
    switch (typeof (str)) {
        case "number":
            return true;
        case "string":
            var reg = /^[0-9]+.?[0-9]*$/;
            return reg.test(str);
        default:
            return false;
    }
}

//获取相对高度
var offsetPageSize = function (obj) {
    var x = 0;
    var y = 0;
    var el = obj;
    while (el.offsetParent) {
        x += el.offsetLeft;
        y += el.offsetTop;
        el = el.offsetParent
    }
    return { "Left": x, "Top": y };
}



//获取页面高度
var pageSize = function () {
    var _rootEl = (document.compatMode == "CSS1Compat"
    ? document.documentElement
    : document.body);
    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = _rootEl.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY
    } else if (_rootEl.scrollHeight > _rootEl.offsetHeight) {
        xScroll = _rootEl.scrollWidth;
        yScroll = _rootEl.scrollHeight + 20
    } else {
        xScroll = _rootEl.offsetWidth;
        yScroll = _rootEl.offsetHeight
    }
    var windowWidth, windowHeight;
    if (self.innerHeight) {
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight
    } else if (_rootEl && _rootEl.clientHeight) {
        windowWidth = _rootEl.clientWidth;
        windowHeight = _rootEl.clientHeight
    } else if (_rootEl) {
        windowWidth = _rootEl.clientWidth;
        windowHeight = _rootEl.clientHeight
    }
    if (yScroll < windowHeight) {
        pageHeight = windowHeight
    } else {
        pageHeight = _rootEl.scrollHeight
    }
    if (xScroll < windowWidth) {
        pageWidth = windowWidth
    } else {
        pageWidth = xScroll
    }
    return { "PageWidth": pageWidth, "PageHeight": pageHeight, "WindwoWidth": windowWidth, "WindowHeight": windowHeight };
}
//获取滚动条高度
var getScrollTop = function () {
    if ('pageYOffset' in window) {
        return window.pageYOffset;
    } else if (document.compatMode === "BackCompat") {
        return document.body.scrollTop;
    } else {
        return document.documentElement.scrollTop;
    }
}

function clone(oldObj) { //复制对象方法
    if (typeof (oldObj) != 'object') return oldObj;
    if (oldObj == null) return oldObj;
    var newObj = new Object();
    for (var i in oldObj)
        newObj[i] = clone(oldObj[i]);
    return newObj;
};
function extend() { //扩展对象
    var args = arguments;
    if (args.length < 2) return;
    var temp = clone(args[0]); //调用复制对象方法
    for (var n = 1; n < args.length; n++) {
        for (var i in args[n]) {
            temp[i] = args[n][i];
        }
    }
    return temp;
}

//是否包含元素
Array.prototype.contains = function (obj) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == obj)
            return true
    }
    return false;
}
//移除指定元素
Array.prototype.remove = function (obj) {
    var k = 0;
    for (var i = 0; i < this.length; i++) {
        if (this[i] == obj) {
            k++;
            continue;
        }
        this[i - k] = this[i];
    }
    this.length = this.length - k;
}
//遍历
Array.prototype.each = function (callback) {
    if (typeof (callback) == "function") {
        for (var i = 0; i < this.length; i++) {
            callback(this[i], i);
        }
    }
}


$.setCookie = function (name, value, option) {
    var str = name + '=' + escape(value);
    if (option) {
        if (option.expireHours) {
            var d = new Date();
            d.setTime(d.getTime() + option.expireHours * 3600 * 1000);
            str += '; expires=' + d.toGMTString();
        }
        if (option.path) str += '; path=' + option.path; else str += '; path=/';
        if (option.domain) str += '; domain=' + option.domain;
        if (option.secure) str += '; true';
    }
    document.cookie = str;
};

$.getCookie = function (name, defaultValue) {
    var coObj = document.cookie, coLen = coObj.length, start = 0, end = 0;
    if (coLen > 0) {
        start = coObj.indexOf(name + "=");
        if (start != -1) {
            start = start + name.length + 1;
            end = coObj.indexOf(";", start);
            if (end == -1) end = coLen;
            return unescape(coObj.substring(start, end));
        }
    }
    if (typeof (defaultValue) == "undefined")
        return "";
    return defaultValue;
};
$.LoadJs = function (o, callback) {
    if (!o) return;

    var script, abort = null, abortTime,
        head = document.head || document.getElementsByTagName("head")[0] || document.documentElement;

    script = document.createElement("script");
    script.async = o.type.toLowerCase();
    if (o.scriptCharset) {
        script.charset = o.scriptCharset;
    }
    script.src = o.url;
    if (o.timeout > 0) abortTime = setTimeout(function () {
        abort();
        callback && callback(408, "failure");
    }, o.timeout);
    script.onload = script.onreadystatechange = function (_, isAbort) {
        if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
            script.onload = script.onreadystatechange = null;
            if (head && script.parentNode) {
                head.removeChild(script);
            }
            script = undefined;
            if (!isAbort) {
                clearTimeout(abortTime);
                callback && callback(200, "success");
            }
        }
    };
    script.onerror = function () {
        abort();
        callback && callback(500, "failure");
    }
    head.insertBefore(script, head.firstChild);

    abort = function () {
        if (script) {
            script.onload(0, 1);
        }
    }
};

/*
Append File:/che168/js/Lazyload_v2.js
*/
var Class = {
    create: function () {
        return function () {
            this.initialize.apply(this, arguments);
        }
    }
}
Object.extend = function (destination, source) {
    for (var property in source) {
        destination[property] = source[property];
    }
    return destination;
}
Function.prototype.bind = function (bindObj, args) {
    var _self = this;
    return function () {
        return _self.apply(bindObj, [].concat(args));
    }
}
var Lazyload = function () { };
Lazyload = Class.create();
Lazyload.prototype = {
    options: {
        defaultimage: "",
        src2: "lazyload",
        ImgList: []
    },
    imageCount: 0,
    $: function (id) {
        if (typeof id != "string") return id;
        return document.getElementById(id);
    },
    initialize: function () {

        Object.extend(this.options, arguments[0]);

        if (typeof this.options.ImgList == 'string') {
            var obj = this.$(this.options.ImgList);
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].getAttribute(this.options.src2) != "") {
                    this.options.ImgList.push(obj[i]);
                }
            }
        }
        if (this.options.ImgList.length == 0) return;
        this.imageCount = this.options.ImgList.length;
        Autohome.events.addEvent(window, this.loaded.bind(this), "scroll");
        Autohome.events.addEvent(window, this.loaded.bind(this), "touchend");
    },
    loaded: function () {
        var pageheight = getScrollPos()[0] + getPageSize()[3];
        var imgsrc = "";
        var flag = IsLazyLoad();
        for (var img = 0; img < this.options.ImgList.length; img++) {
            if (flag == true) {
                if (this.getXY(this.options.ImgList[img]).Top < pageheight + 500) {
                    //window.status = this.options.ImgList[img];                    
                    imgsrc = this.options.ImgList[img].getAttribute(this.options.src2);
                    this.options.ImgList[img].src = imgsrc;
                    //delete this.options.ImgList[img];
                    this.imageCount--;
                }
                else {
                    break;
                }
            }
            else {
                imgsrc = this.options.ImgList[img].getAttribute(this.options.src2);
                this.options.ImgList[img].src = imgsrc;
                this.imageCount--;
            }
        }
        if (this.imageCount == 0) Autohome.events.removeEvent(window, this.loaded, "scroll");
    },
    //得到对象的Left 和 Top
    getXY: function (element) {
        var x = 0;
        var y = 0;
        var el = this.$(element);
        while (el.offsetParent) {
            x += el.offsetLeft;
            y += el.offsetTop;
            el = el.offsetParent
        }
        return { "Left": x, "Top": y };
    }
}



var Autohome = {};
Autohome.events = {}; //事件
Autohome.events.getEvent = function () {
    return window.event
};
if (!window.isIE) {
    Autohome.events.getEvent = function () {
        var o = arguments.callee.caller;
        var e;
        var n = 0;
        while (o != null && n < 40) {
            e = o.arguments[0];
            if (e && (e.constructor == Event || e.constructor == MouseEvent)) {
                return e
            }
            n++;
            o = o.caller
        }
        return e
    }
}

//删除事件
Autohome.events.removeEvent = function (obj, func, evType) {
    var elm = obj;
    if ("function" != typeof func) return;

    if (elm.addEventListener) {
        if (evType.substr(0, 2) == "on") evType = evType.substring(2);
        elm.removeEventListener(evType, func, false)
    } else if (elm.attachEvent) {
        if (evType.substr(0, 2) != "on") evType = "on" + evType;
        elm.detachEvent(evType, func)
    }
};
//加事件
Autohome.events.addEvent = function (obj, func, evType) {
    if (obj.attachEvent) {
        if (evType.substr(0, 2) != "on") evType = "on" + evType;
        obj.attachEvent(evType, func);
        return true;
    }
    if (obj.addEventListener) {
        if (evType.substr(0, 2) == "on") evType = evType.substring(2);
        obj.addEventListener(evType, func, false);
        return true;
    }
};

;
getPageSize = function () {
    var _rootEl = (document.compatMode == "CSS1Compat"
			? document.documentElement
			: document.body);
    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = _rootEl.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY
    } else if (_rootEl.scrollHeight > _rootEl.offsetHeight) {
        xScroll = _rootEl.scrollWidth;
        yScroll = _rootEl.scrollHeight + 20
    } else {
        xScroll = _rootEl.offsetWidth;
        yScroll = _rootEl.offsetHeight
    }
    var windowWidth, windowHeight;
    if (self.innerHeight) {
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight
    } else if (_rootEl && _rootEl.clientHeight) {
        windowWidth = _rootEl.clientWidth;
        windowHeight = _rootEl.clientHeight
    } else if (_rootEl) {
        windowWidth = _rootEl.clientWidth;
        windowHeight = _rootEl.clientHeight
    }
    if (yScroll < windowHeight) {
        pageHeight = windowHeight
    } else {
        pageHeight = _rootEl.scrollHeight
    }
    if (xScroll < windowWidth) {
        pageWidth = windowWidth
    } else {
        pageWidth = xScroll
    }
    return [pageWidth, pageHeight, windowWidth, windowHeight]
};
//获取页面的滚动条值[]
getScrollPos = function (oDocument) {
    var oDocument = oDocument || document;
    return [
			Math.max(oDocument.documentElement.scrollTop,
					oDocument.body.scrollTop),
			Math.max(oDocument.documentElement.scrollLeft,
					oDocument.body.scrollLeft),
			Math.max(oDocument.documentElement.scrollWidth,
					oDocument.body.scrollWidth),
			Math.max(oDocument.documentElement.scrollHeight,
					oDocument.body.scrollHeight)]
};

function IsLazyLoad() {
    var ua = navigator.userAgent.toLowerCase();
    //document.getElementById("msg").innerHTML = ua + "Zhi Shi=" + s;
    if (ua.indexOf("ipad") > -1 || ua.indexOf("ipod") > -1 || ua.indexOf("iphone") > -1 || ua.indexOf("linux i686") > -1) {
        //document.getElementById("msg").innerHTML = ua;
        return false;
    }
    else {
        return true;
    }
};

/*
Append File:/2sc/pc/UsedCarList/v6/SolrFilterInfo.js
*/
function SolrFilterInfo() {   /// <summary>
    /// 搜索返回结果集的行数
    /// </summary>
    this.Rows = 0;
    /// <summary>
    /// 结果分页页码
    /// </summary>
    this.PageIdx = 0;
    /// <summary>
    /// 搜索关键词
    /// </summary>
    this.KeyWord = "";
    /// <summary>
    /// 车源Id
    /// </summary>
    this.InfoId = 0;
    /// <summary>
    /// 地区拼音
    /// </summary>
    this.AreaPinyin = "";
    /// <summary>
    /// 地区名称
    /// </summary>
    this.AreaName = "";
    /// <summary>
    /// 地区Id
    /// </summary>
    this.AreaId = 0;
    /// <summary>
    /// 省份Id
    /// </summary>
    this.ProvinceId = 0;
    /// <summary>
    /// 城市Id
    /// </summary>
    this.CityId = 0;
    /// <summary>
    /// 价格区间（开始）
    /// </summary>
    this.MinPrice = 0;
    /// <summary>
    /// 价格区间（结束）
    /// </summary>
    this.MaxPrice = 0;
    /// <summary>
    /// 是否为自定义价格区间（0：默认为固定价格区间，但如果价格区间不在固定区间时自动切换为自定义区间、1：自定义价格区间、2：固定价格区间）
    /// </summary>
    this.CustomPrice = 0;
    /// <summary>
    /// 级别拼音
    /// </summary>
    this.ClassPinyin = "";
    /// <summary>
    /// 级别Id
    /// </summary>
    this.ClassId = [];
    /// <summary>
    /// 品牌拼音
    /// </summary>
    this.BrandPinyin = "";
    /// <summary>
    /// 品牌Id
    /// </summary>
    this.BrandId = 0;
    /// <summary>
    /// 车系拼音
    /// </summary>
    this.SeriesPinyin = 0;
    /// <summary>
    /// 车系Id
    /// </summary>
    this.SeriesId = [];
    /// <summary>
    /// 车系年代款id
    /// </summary>
    this.SeriesYearId = [];
    /// <summary>
    /// 车型id
    /// </summary>
    this.SpecId = [];
    /// <summary>
    /// 车龄（开始）
    /// </summary>
    this.MinAge = 0;
    /// <summary>
    /// 车龄（结束）
    /// </summary>
    this.MaxAge = 0;
    /// <summary>
    /// 里程
    /// </summary>
    this.MileageId = 0;
    /// <summary>
    /// 量程（开始）
    /// </summary>
    this.MinMileage = 0;
    /// <summary>
    /// 量程（结束）
    /// </summary>
    this.MaxMileage = 0;
    /// <summary>
    /// 驱动方式
    /// </summary>
    this.PowerTrain = [];
    /// <summary>
    /// 变速箱
    /// </summary>
    this.Gearbox = 0;
    /// <summary>
    /// 排量id
    /// </summary>
    this.DisplacementId = [];
    /// <summary>
    /// 排量（开始）
    /// </summary>
    this.MinDisplacement = [];
    /// <summary>
    /// 排量（结束）
    /// </summary>
    this.MaxDisplacement = [];
    /// <summary>
    /// 车身结构
    /// </summary>
    this.Structure = [];
    /// <summary>
    /// 国别（1：合资，2：自主，3：进口）
    /// </summary>
    this.CountryType = [];

    /// <summary>
    /// 国籍
    /// </summary>
    this.Nationality = [];
    /// <summary>
    /// 燃料
    /// </summary>
    this.Fuel = [];
    /// <summary>
    /// 座位
    /// </summary>
    this.Seat = [];
    /// <summary>
    /// 配置
    /// </summary>
    this.Option = [];
    /// <summary>
    /// 颜色
    /// </summary>
    this.Color = [];
    /// <summary>
    /// 原厂质保期内
    /// </summary>
    this.HasWarranty = 0;
    /// <summary>
    /// 延长质保
    /// </summary>
    this.ExtendRepair = 0;
    /// <summary>
    /// 是否是准新车
    /// </summary>
    this.IsNewCar = 0;
    /// <summary>
    /// 是否有图
    /// </summary>
    this.HasPhoto = 0;
    /// <summary>
    /// 销售状态（5：已售，9：在售）
    /// </summary>
    this.SellState = 0;
    /// <summary>
    /// 是否有保证金
    /// </summary>
    this.BailType = 0;
    /// <summary>
    /// 车源类型（0：全部、1：个人、2：商家、3：认证商家、4保障车）
    /// </summary>
    this.Source = [];
    /// <summary>
    /// 认证商家id
    /// </summary>
    this.CreditId = 0;
    /// <summary>
    /// 是否只包含特价车(1：是，0：否）
    /// </summary>
    this.HasDiscountPrice = 0;

    /// <summary>
    /// 0：大图模式，1：列表视图模式
    /// </summary>
    this.ListView = 0;
    /// <summary>
    /// 排序依赖的字段
    /// </summary>
    this.Sort = 0;
    /// <summary>
    /// 展开的一组条件（该值是一个按位“与”计算后的一个值。1：量程，2：排量，4：变速箱，8：驱动方式，16：属性（国别），32：车身结构，64：颜色 128:国籍 256 燃料 512 座位 1024 配置 ）
    /// </summary>
    this.Expando = 0;
    /// <summary>
    /// （已经弃用）网页定位标识，1：筛选条件位置，2：结果列表位置，3：TAB位置
    /// </summary>
    this.PagePosition = 0;
    /// <summary>
    /// 最后一次选择的条件，21：里程、22：排量、23：变速箱。应用场景：当用户点击“里程...”按钮时，展示出来的量程高亮显示。
    /// </summary>
    this.LastFilter = 0;
    /// <summary>
    /// 商家ID
    /// </summary>
    this.DealerId = 0;
    /// <summary>
    /// vin码状态 状态是否选中
    /// </summary>
    this.IsVincodeStatus = 0;
    /// <summary>
    /// 是否多选
    /// </summary>
    this.IsMultipleSelect = false;
    /// <summary>
    /// 排放标准
    /// </summary>
    this.Environmental = [];
    ///外部链接（美团、违章）
    this.SourceName = "";
}


SolrFilterInfo.prototype.BuilderUrl = function (pv) {
    var url = new StringBuffer();
    //segments
    url.Append('/').Append(this.AreaPinyin ? this.AreaPinyin : "china");
    var urlnothing = true;
    if (this.BrandPinyin) {
        urlnothing = false;
        url.Append('/').Append(this.BrandPinyin);
        //url segments:品牌/车系/s车型
        if (this.SeriesPinyin) {
            url.Append('/').Append(this.SeriesPinyin);
            if (this.SpecId.length > 0) {
                url.Append('/').Append('s').Append(this.SpecId.join("-"));
            }
        }
            //url segments:品牌/级别
        else if (this.ClassPinyin) {
            url.Append('/').Append(this.ClassPinyin);
        }
    } else if (this.ClassPinyin && !this.SeriesPinyin) {
        urlnothing = false;
        url.Append('/').Append(this.ClassPinyin);
    }
    if (this.MinPrice > 0 || this.MaxPrice > 0) //价格
    {
        urlnothing = false;
        url.Append('/').Append(this.MinPrice + "_" + this.MaxPrice);
    }
    url.Append('/');

    //....../a0_0msdgscncgpiltocspex/
    if (this.BuidParam(url)) {
        urlnothing = false;
    }

    //china/list/
    if (urlnothing) {
        url.Append("list/");
    }
    //kw
    this.KeyWord = $("#filterKw").val();
    if (!string.IsNullOrEmpty(this.KeyWord)) {
        url.Append("?kw=").Append(this.KeyWord);
    }
    if (pv) {
        if (string.IsNullOrEmpty(this.SourceName)) {
            url.Append("#pvareaid=" + pv);
        } else {
            url.Append("?sourcename=" + this.SourceName + "&pvareaid=" + pv)
        }
    } else {
        if (!string.IsNullOrEmpty(this.SourceName)) {
            url.Append("?sourcename=" + this.SourceName);
        }
    }
    return url.ToString();
};
SolrFilterInfo.prototype.BuidParam = function (output) {

    if (this.MinAge > 0 || this.MaxAge > 0 || this.MileageId > 0 || this.MinMileage > 0 || this.MaxMileage > 0 ||
        this.Source.length > 0 || this.DisplacementId.length > 0 || this.Gearbox > 0 || this.Environmental.length > 0 ||
        this.Structure.length > 0 || this.Color.length > 0 || this.IsNewCar > 0 ||
        this.ExtendRepair > 0 || this.ListView > 0 ||
        this.CreditId > 0 ||
        this.CountryType.length > 0 || this.PageIdx > 0 ||
        this.CustomPrice > 0 ||
        this.PowerTrain.length > 0 || this.SeriesYearId.length > 0 ||
        this.Sort > 0 || this.DealerId > 0 || this.Nationality.length > 0 || this.Seat.length > 0 || this.Fuel.length > 0 || this.Option.length > 0 || this.SellState != 9) {
        //大把的参数...
        output.Append('a').Append(this.MinAge).Append('_').Append(this.MaxAge);
        if (this.MinMileage > 0 || this.MaxMileage > 0) {
            output.Append('m');
        } else {
            output.Append('m').Append(this.MileageId > 0 ? this.MileageId : string.Empty);
        }
        output.Append('s').Append(this.Source.length > 0 ? this.Source.join("-") : string.Empty);
        output.Append('d');
        if (this.DisplacementId.length > 0) {
            output.Append(this.DisplacementId.join('-'));
        }
        output.Append('g').Append(this.Gearbox > 0 ? this.Gearbox : string.Empty);

        output.Append('s');
        if (this.Structure.length > 0) {
            output.Append(this.Structure.join('-'));
        }
        output.Append('c');
        if (this.Color.length > 0) {
            output.Append(this.Color.join('-'));
        }
        output.Append("nc").Append(this.IsNewCar > 0 ? this.IsNewCar : string.Empty)
            .Append("gp").Append(this.ExtendRepair > 0 ? this.ExtendRepair : string.Empty)
            .Append('i').Append(this.SellState == 9 ? "1" : this.IsAroundCityCar ? "0" : string.Empty)
            .Append("lt").Append(this.ListView > 0 ? this.ListView : string.Empty)
            .Append('o').Append(this.Sort > 0 ? this.Sort : string.Empty)
            .Append('c').Append(this.CreditId > 0 ? this.CreditId : string.Empty)
            .Append('s')
            .Append('p');
        output.Append(this.PageIdx);
        //ex
        output.Append("ex");
        if (this.CountryType.length > 0) {

            output.Append('c')
            output.Append(this.CountryType.join('-'));

        }
        if (this.HasDiscountPrice > 0) //是否特价车
        {
            output.Append('d').Append(this.HasDiscountPrice);
        }
        if (this.BailType > 0) //是否保证金
        {
            output.Append('t').Append(this.BailType);
        }
        if (this.HasPhoto > 0) //是否有照片
        {
            output.Append('p').Append(this.HasPhoto);
        }
        if (this.HasWarranty > 0) //是否原厂质保
        {
            output.Append('w').Append(this.HasWarranty);
        }
        if (this.CustomPrice > 0) {
            output.Append('u').Append(this.CustomPrice);
        }
        if (this.Expando > 0) {
            output.Append('e').Append(this.Expando);
        }
        if (this.PowerTrain.length > 0) {
            output.Append('r');
            output.Append(this.PowerTrain.join('-'));
        }

        if (this.SeriesYearId.length > 0) {

            output.Append('s');
            output.Append(this.SeriesYearId.join('-'));

        }

        if (this.DealerId > 0) {
            output.Append('g').Append(this.DealerId);
        }
        if (this.Nationality.length > 0) {
            output.Append('n').Append(this.Nationality.join('-'));
        }
        if (this.Fuel.length > 0) {
            output.Append('f').Append(this.Fuel.join('-'));
        }
        if (this.Seat.length > 0) {
            output.Append("z").Append(this.Seat.join('-'));
        }
        if (this.Option.length > 0) {
            output.Append("l").Append(this.Option.join('-'));
        }
        if (this.MinMileage > 0 || this.MaxMileage > 0) {
            output.Append("m").Append(this.MinMileage + "_" + this.MaxMileage);
        }
        if (this.ClassId.length > 0 && !string.IsNullOrEmpty(this.SeriesPinyin)) {
            output.Append("x").Append(this.ClassId.join('-'));
        }
        if (this.Environmental.length > 0) {
            output.Append('h').Append(this.Environmental.join('-'));
        }
        output.Append('/');
        return true;
    }
    return false;
};

SolrFilterInfo.prototype.ToString = function () {
    var builder = new StringBuffer()
        .Append("area=").Append(this.AreaPinyin).Append('&')
        .Append("brand=").Append(this.BrandPinyin).Append('&')
        .Append("ls=").Append(string.IsNullOrEmpty(this.SeriesPinyin) ? this.ClassPinyin : this.SeriesPinyin).Append('&')
        .Append("spec=").Append(string.Join("-", this.SpecId)).Append('&')
        .Append("minPrice=").Append(this.MinPrice).Append('&')
        .Append("maxPrice=").Append(this.MaxPrice).Append('&')
        .Append("minRegisteAge=").Append(this.MinAge).Append('&')
        .Append("maxRegisteAge=").Append(this.MaxAge).Append('&')
        .Append("MileageId=").Append(this.MileageId).Append('&')
        .Append("disp=").Append(string.Join("-", this.DisplacementId)).Append('&')
        .Append("stru=").Append(string.Join("-", this.Structure)).Append('&')
        .Append("gb=").Append(this.Gearbox).Append('&')
        .Append("color=").Append(string.Join("-", this.Color)).Append('&')
        .Append("source=").Append(string.Join("-", this.Source)).Append('&')
        .Append("listview=").Append(this.ListView).Append('&')
        .Append("sell=").Append(this.SellState == 9 ? 1 : 0).Append('&')
        .Append("newCar=").Append(this.IsNewCar).Append('&')
        .Append("credit=").Append(this.CreditId).Append('&')
        .Append("sort=").Append(this.Sort).Append('&')
        .Append("extrepair=").Append(this.ExtendRepair).Append("&")
        .Append("page=").Append(this.PageIdx).Append("&")
        .Append("ex=").AppendFormat("c{0}d{11}t{1}p{2}w{3}r{4}u{5}e{6}s{7}a{8}o{9}i{10}g{12}n{13}f{14}z{15}l{16}k{17}v{18}a{19}m{20}x{21}", string.Join("-", this.CountryType), this.BailType, this.HasPhoto, this.HasWarranty, string.Join("-", this.PowerTrain), this.CustomPrice, this.Expando, string.Join("-", this.SeriesYearId), this.PagePosition, this.LastFilter, this.InfoId, this.HasDiscountPrice, this.DealerId, string.Join("-", this.Nationality), string.Join("-", this.Fuel), string.Join("-", this.Seat), string.Join("-", this.Option), this.IsCheckCar, this.IsVincodeStatus, this.Activitytype, this.MinMileage + "_" + this.MaxMileage, this.ClassId.join('-'));
    if (!string.IsNullOrEmpty(this.SourceName))
        builder.Append("&sourcename=" + this.SourceName);

    return builder.ToString();
}


SolrFilterInfo.prototype.ToJString = function () {
    var builder = new StringBuffer()
        .Append("area=").Append(this.AreaPinyin).Append('&')
        .Append("brand=").Append(this.BrandPinyin).Append('&')
    if (string.IsNullOrEmpty(this.SeriesPinyin))
        builder.Append("class=").Append(this.ClassPinyin).Append("&");
    else {
        builder.Append("series=").Append(this.SeriesPinyin).Append("&");
    }
    //  .Append("ls=").Append(string.IsNullOrEmpty(this.SeriesPinyin) ? this.ClassPinyin : this.SeriesPinyin).Append('&')
    builder.Append("spec=").Append(string.Join("-", this.SpecId)).Append('&')
        .Append("minPrice=").Append(this.MinPrice).Append('&')
        .Append("maxPrice=").Append(this.MaxPrice).Append('&')
        .Append("minRegisteAge=").Append(this.MinAge).Append('&')
        .Append("maxRegisteAge=").Append(this.MaxAge).Append('&')
        .Append("MileageId=").Append(this.MileageId).Append('&')
        .Append("disp=").Append(string.Join("-", this.DisplacementId)).Append('&')
        .Append("stru=").Append(string.Join("-", this.Structure)).Append('&')
        .Append("gb=").Append(this.Gearbox).Append('&')
        .Append("color=").Append(string.Join("-", this.Color)).Append('&')
        .Append("source=").Append(string.Join("-", this.Source)).Append('&')
        .Append("listview=").Append(this.ListView).Append('&')
        .Append("sell=").Append(this.SellState == 9 ? 1 : 0).Append('&')
        .Append("newCar=").Append(this.IsNewCar).Append('&')
        .Append("credit=").Append(this.CreditId).Append('&')
        .Append("sort=").Append(this.Sort).Append('&')
        .Append("page=").Append(this.PageIdx).Append("&")
        .Append("extrepair=").Append(this.ExtendRepair).Append("&")
        .Append("ex=").AppendFormat("c{0}d{11}t{1}p{2}w{3}r{4}u{5}e{6}s{7}a{8}o{9}i{10}g{12}n{13}f{14}z{15}l{16}k{17}v{18}a{19}m{20}x{21}", string.Join("-", this.CountryType), this.BailType, this.HasPhoto, this.HasWarranty, string.Join("-", this.PowerTrain), this.CustomPrice, this.Expando, string.Join("-", this.SeriesYearId), this.PagePosition, this.LastFilter, this.InfoId, this.HasDiscountPrice, this.DealerId, string.Join("-", this.Nationality), string.Join("-", this.Fuel), string.Join("-", this.Seat), string.Join("-", this.Option), this.IsCheckCar, this.IsVincodeStatus, this.Activitytype, this.MinMileage + "_" + this.MaxMileage, this.ClassId.join('-'));
    if (!string.IsNullOrEmpty(this.SourceName)) {
        builder.Append("&sourcename=" + this.SourceName);
    }
    return builder.ToString();
}

SolrFilterInfo.Convert = function (str) {
    var solrFilterInfo = new SolrFilterInfo();
    if (str.indexOf("&") > -1) {
        var array = str.split("&");
        for (var j = 0; j < array.length; j++) {
            var temparray = array[j].split("=");
            switch (temparray[0]) {
                case "sourcename":
                    solrFilterInfo.SourceName = temparray[1];
                    break;
                case "extrepair":
                    solrFilterInfo.ExtendRepair = Tool.GetNumber(temparray[1]);
                    break;
                case "row":
                    solrFilterInfo.Rows = temparray[1];
                    break;
                case "area":
                    solrFilterInfo.AreaPinyin = temparray[1];
                    break;
                case "brand":
                    solrFilterInfo.BrandPinyin = temparray[1];
                    break;
                case "series":
                    solrFilterInfo.SeriesPinyin = temparray[1];
                    break;
                case "class":
                    solrFilterInfo.ClassPinyin = temparray[1];
                    break;
                case "spec":
                    {
                        var ary = temparray[1].split("-");
                        var k = 0;
                        for (var i = 0; i < ary.length; i++) {
                            var tempid = Tool.GetNumber(ary[i]);
                            if (tempid != 0) {
                                solrFilterInfo.SpecId[k] = tempid;
                                k++;
                            }
                        }
                        break;
                    }
                case "minPrice":
                    solrFilterInfo.MinPrice = Tool.GetNumber(temparray[1]);
                    break;
                case "maxPrice":
                    solrFilterInfo.MaxPrice = Tool.GetNumber(temparray[1]);
                    break;
                case "minRegisteAge":
                    solrFilterInfo.MinAge = Tool.GetNumber(temparray[1]);
                    break;
                case "maxRegisteAge":
                    solrFilterInfo.MaxAge = Tool.GetNumber(temparray[1]);
                    break;
                case "MileageId":
                    solrFilterInfo.MileageId = Tool.GetNumber(temparray[1]);
                    break;
                case "disp":
                    {
                        var ary = temparray[1].split("-");
                        var k = 0;
                        for (var i = 0; i < ary.length; i++) {
                            var tempid = Tool.GetNumber(ary[i]);
                            if (tempid != 0) {
                                solrFilterInfo.DisplacementId[i] = tempid;
                                k++;
                            }
                        }
                        break;
                    }
                case "stru":
                    {
                        var ary = temparray[1].split("-");
                        var k = 0;
                        for (var i = 0; i < ary.length; i++) {
                            var tempid = Tool.GetNumber(ary[i]);
                            if (tempid != 0) {
                                solrFilterInfo.Structure[i] = tempid;
                                k++;
                            }
                        }
                        break;
                    }
                    break;
                case "gb":
                    solrFilterInfo.Gearbox = Tool.GetNumber(temparray[1]);
                    break;
                case "color":
                    {
                        var ary = temparray[1].split("-");
                        var k = 0;
                        for (var i = 0; i < ary.length; i++) {
                            var tempid = Tool.GetNumber(ary[i]);
                            if (tempid != 0) {
                                solrFilterInfo.Color[i] = tempid;
                                k++;
                            }
                        }
                        break;
                    }
                case "source":
                    {
                        var ary = temparray[1].split("-");
                        var k = 0;
                        for (var i = 0; i < ary.length; i++) {
                            var tempid = Tool.GetNumber(ary[i]);
                            if (tempid != 0) {
                                solrFilterInfo.Source[i] = tempid;
                                k++;
                            }
                        }
                        break;
                    }
                case "listview":
                    solrFilterInfo.ListView = Tool.GetNumber(temparray[1]);
                    break;
                case "sell":
                    solrFilterInfo.SellState = Tool.GetNumber(temparray[1]) == 1 ? 9 : 0;
                    break;
                case "newCar":
                    solrFilterInfo.IsNewCar = Tool.GetNumber(temparray[1]);
                    break;
                case "credit":
                    solrFilterInfo.CreditId = Tool.GetNumber(temparray[1]);
                    break;
                case "page":
                    solrFilterInfo.PageIdx = Tool.GetNumber(temparray[1]);
                    break;
                case "sort":
                    solrFilterInfo.Sort = Tool.GetNumber(temparray[1]);
                    break;
                case "ex":
                    var reg = new RegExp("(c\\d(-\\d)*)|(d\\d)|(t\\d)|(p\\d)|(w\\d)|(r\\d(-\\d)*)|(u\\d)|(e\\d+)|(s\\d+(-\\d+)*)|(o\\d+)|(i\\d+)|(g\\d+)|(n\\d+(-\\d+)*)|(f\\d+(-\\d+)*)|(z\\d+(-\\d+)*)|(l\\d+(-\\d+)*)|(k\\d+)|(v\\d+)|(a\\d+)|(m\\d+_\\d+)|(x\\d+(-\\d+)*)|(h\\d+(-\\d+)*)", "g");
                    var mathm = temparray[1].match(reg);
                    if (ary != null) {
                        for (var k = 0; k < mathm.length; k++) {
                            if (mathm[k].Length <= 1) {
                                continue;
                            }
                            var value = mathm[k].substring(1);
                            var char = mathm[k].substring(0, 1);
                            switch (char) {
                                case 'c': //国别
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0)
                                                solrFilterInfo.CountryType.push(temp);
                                        }
                                    }
                                    break;
                                case 'd': //特价车
                                    solrFilterInfo.HasDiscountPrice = Tool.GetNumber(value);
                                    break;
                                case 't': //保证金
                                    solrFilterInfo.BailType = Tool.GetNumber(value);
                                    break;
                                case 'p': //是否有图片
                                    solrFilterInfo.HasPhoto = Tool.GetNumber(value);
                                    break;
                                case 'w': //原厂质保
                                    solrFilterInfo.HasWarranty = Tool.GetNumber(value);
                                    break;
                                case 'r': //驱动方式
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0)
                                                solrFilterInfo.PowerTrain.push(temp);
                                        }
                                    }
                                    break;
                                case 'u': //是否之定义价钱
                                    solrFilterInfo.CustomPrice = Tool.GetNumber(value);
                                    break;
                                case 'e': //展开条件
                                    {
                                        solrFilterInfo.Expando = Tool.GetNumber(value);
                                        break;
                                    }
                                case 's': //车型年代款
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0)
                                                solrFilterInfo.SeriesYearId.push(temp);
                                        }
                                    }
                                    break;
                                case 'o': //最后一次选着条件
                                    solrFilterInfo.LastFilter = Tool.GetNumber(value);
                                    break;
                                case 'i': //车源id
                                    solrFilterInfo.InfoId = Tool.GetNumber(value);
                                    break;
                                case 'g': //商家id
                                    solrFilterInfo.DealerId = Tool.GetNumber(value);
                                    break;
                                case 'n': //国籍
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0)
                                                solrFilterInfo.Nationality.push(temp);
                                        }
                                    }
                                    break;
                                case 'f': //燃料
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0) {
                                                solrFilterInfo.Fuel.push(temp);
                                            }
                                        }
                                    }
                                    break;
                                case 'z': //座位
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0) {
                                                solrFilterInfo.Seat.push(temp);
                                            }
                                        }
                                    }
                                    break;
                                case 'l': //配置
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0) {
                                                solrFilterInfo.Option.push(temp);
                                            }
                                        }

                                    }
                                    break;
                                case 'k'://检车车源
                                    {
                                        solrFilterInfo.IsCheckCar = Tool.GetNumber(value);
                                        break;
                                    }

                                case 'v'://vin码状态
                                    {
                                        solrFilterInfo.IsVincodeStatus = Tool.GetNumber(value);
                                        break;
                                    }
                                case 'a':
                                    solrFilterInfo.Activitytype = Tool.GetNumber(value);
                                    break;
                                case 'm':
                                    {
                                        var val = value.split("_");
                                        solrFilterInfo.MaxMileage = Tool.GetNumber(val[1]);
                                        solrFilterInfo.MinMileage = Tool.GetNumber(val[0]);
                                    }
                                    break;
                                case 'x':
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0) {
                                                solrFilterInfo.ClassId.push(temp);
                                            }
                                        }
                                    }
                                case 'h':
                                    {
                                        var val = value.split('-');
                                        for (var i = val.length - 1; i >= 0; i--) {
                                            var temp = Tool.GetNumber(val[i]);
                                            if (temp > 0) {
                                                solrFilterInfo.Environmental.push(temp);
                                            }
                                        }
                                    }
                            }
                        }
                    }


                    break;
            }
        }
    }
    //kw
    solrFilterInfo.KeyWord = "";
    var KeyWord = $("#filterKw").val();
    if (!string.IsNullOrEmpty(KeyWord)) {
        solrFilterInfo.KeyWord = KeyWord;
    }

    return solrFilterInfo;
}


/*
Append File:/2sc/pc/UsedCarList/v6/UsedCarListNew_v6.js
*/

/*筛选项 列表页的span的click页面跳转的事件处理*/
var Filter = function () {
    function stopEvent() { //阻止冒泡事件
        //取消事件冒泡 
        var e = arguments.callee.caller.arguments[0] || event; //若省略此句，下面的e改为event，IE运行可以，但是其他浏览器就不兼容
        if (e && e.stopPropagation) {
            // this code is for Mozilla and Opera
            e.stopPropagation();
        } else if (window.event) {
            // this code is for IE 
            window.event.cancelBubble = true;
        }
    };

    /*鼠标点击事件 显示和隐藏层事件*/
    function TipClickQuery(tgerDiv, showDiv, tgerDivClass, showDivClass, movecallback, outcallback) {
        if (showDiv.size() > 0) {
            this.init = function () {
                tgerDiv.bind("click", tipClick);
            };

            /*鼠标点击 用户信息提示*/
            var tipClick = function (event) {
                event.stopPropagation();
                $("div.condition-list").attr("style", "z-index:0px");
                if (showDiv.css('display') != "none") {
                    showDiv.hide();
                    if (outcallback) {
                        outcallback(tgerDiv, showDiv);
                    }
                } else {
                    $(".select").removeClass("active");
                    $(".select-option").hide();
                    $("#hotpop").hide();
                    $("#hotmore").html("更多<i class=\"iconfont\">&#xe916;</i>");
                    $("#brandshow").hide();
                    $("#brandmore").html("更多<i class=\"iconfont\">&#xe916;</i>");
                    $("#hotseriesmore").html("更多<i class=\"iconfont\">&#xe916;</i>");
                    $("#series").addClass("fn-hide"); $("#seriespop").addClass("fn-hide");
                    $("#hotserices").removeClass("fn-hide");
                    showDiv.show();
                    if (movecallback) {
                        movecallback(tgerDiv, showDiv);
                    }
                }
            };

            this.init();
        }
    };

    $(function () {
        //FilterConditions.init(); 去掉筛选选项
        GetGoldDealers.init();
        Page.init();
        Hot.init();
        Brand.init();
        Series.init();
        SeriesYear.init();
        Spec.init();
        Price.init();
        FilterOther.Load();
        $(".onclick").click(function () {
            if (!string.IsNullOrEmpty($(this).attr("href"))) {

                window.open($(this).attr("href"), "_blank");
            }
        });
        //点击空白页面，隐藏相关DIV
        $(document).bind("click", function (e) {
            var target = $(e.target);
            if (target.closest(".select,#seriespop").length == 0) {
                $(".select").removeClass("active");
                $(".select-option").hide();
                $("#hotpop").hide();
                $("#hotmore").html("更多<i class=\"iconfont\">&#xe916;</i>");
                $("#brandshow").hide();
                $("#brandmore").html("更多<i class=\"iconfont\">&#xe916;</i>");
                $("div.condition-list").attr("style", "z-index:0px");
                $("#div_registDatePop").hide();
                $("#sh_divSCBrandSeries1_pop").hide();
                $("#sh_divSCBrandSeries2_pop").hide();
                $("#sh_divSCBrandSeries3_pop").hide();
                $(".other-list ul").hide();
                $("#orther-ul-1").show();
                $("#series").addClass("fn-hide");
                $("#hotserices").removeClass("fn-hide");
                $("#seriespop").addClass("fn-hide");
                $(".condition").css({ "position": "inherit" });
                $("#ulother").show(); $("#other-more").html("更多<i class=\"iconfont\">&#xe916;</i>");


            }
        });


    });
    var FilterOther = new Object();
    FilterOther.Isload = false;
    FilterOther.Load = function () {
        var _filterval = $("#filterInfoJs").val();
        var solrfilterinfo = SolrFilterInfo.Convert(_filterval);
        var hid_noresult = $("#hid_noresult");
        var isNoResult = 0;
        if (hid_noresult && hid_noresult.val() == "noresult") {
            isNoResult = 1;
        }
        var IsNoSetFilterCondition = solrfilterinfo.AreaPinyin == "china" && solrfilterinfo.Source.length == 0 && solrfilterinfo.Sort == 0 && solrfilterinfo.InfoId <= 0 && solrfilterinfo.KeyWord == "" &&
                  solrfilterinfo.SeriesYearId.length == 0 && solrfilterinfo.BrandPinyin == "" && solrfilterinfo.SeriesId.length <= 0 && solrfilterinfo.MinPrice <= 0 && solrfilterinfo.MaxPrice <= 0 &&
                  solrfilterinfo.MinAge <= 0 && solrfilterinfo.MaxAge <= 0 && solrfilterinfo.ClassPinyin == "" && solrfilterinfo.MinDisplacement.length <= 0 && solrfilterinfo.MinMileage <= 0 &&
                  solrfilterinfo.MaxMileage <= 0 &&
                 solrfilterinfo.SpecId.length == 0 && solrfilterinfo.PowerTrain.length == 0 && solrfilterinfo.Gearbox == 0 && solrfilterinfo.Structure.length == 0 &&
                  solrfilterinfo.CountryType.length == 0 && solrfilterinfo.Nationality.length == 0 &&
                  solrfilterinfo.Fuel.length == 0 && solrfilterinfo.Seat.length == 0 && solrfilterinfo.Option.length == 0 && solrfilterinfo.Color.length == 0 && solrfilterinfo.HasWarranty == 0 &&
                  solrfilterinfo.ExtendRepair == 0 && solrfilterinfo.IsNewCar == 0 && solrfilterinfo.HasPhoto == 0 &&
                  solrfilterinfo.BailType == 0 && solrfilterinfo.DealerId == 0 && solrfilterinfo.IsVincodeStatus == 0 && solrfilterinfo.Environmental.length == 0;
        if (IsNoSetFilterCondition || isNoResult == 1) {
            var uli = $("#ulother li");
            var li = uli.eq(5);
            var div = li.find(".select-option");
            if (div) {
                div.attr("style", "left: inherit; right: 0px; ");
            }
            var li2 = uli.eq(12);
            div = li2.find(".select-option");
            if (div) {
                div.attr("style", "left: inherit; right: 0px; ");
            }

            var stringBuder = "";
            stringBuder += '<ul class=\"condition-con fn-clear\" id=\"orther-ul-1\">';
            stringBuder += '<li>' + uli.eq(0).html() + '</li>';
            stringBuder += '<li>' + uli.eq(1).html() + '</li>';
            stringBuder += '<li>' + uli.eq(2).html() + '</li>';
            stringBuder += '<li>' + uli.eq(3).html() + '</li>';
            stringBuder += '<li>' + uli.eq(4).html() + '</li>';
            stringBuder += '<li>' + uli.eq(5).html() + '</li>';
            stringBuder += '<a href=\"javascript:void(0)\" id=\"other-more\" class=\"more\">更多<i class=\"iconfont\">&#xe916;</i></a></ul>';
            stringBuder += '<ul class=\"condition-con fn-clear fn-hide\">';
            stringBuder += '<li>' + uli.eq(6).html() + '</li>';
            stringBuder += '<li>' + uli.eq(7).html() + '</li>';
            stringBuder += '<li>' + uli.eq(8).html() + '</li>';
            stringBuder += '<li>' + uli.eq(9).html() + '</li>';
            stringBuder += '<li>' + uli.eq(10).html() + '</li>';
            stringBuder += '<li>' + uli.eq(11).html() + '</li>';
            stringBuder += '<li>' + uli.eq(12).html() + '</li>';
            stringBuder += '<li>' + uli.eq(13).html() + '</li>';
            stringBuder += '</ul>';

            $(".other-list").html(stringBuder);
            $("#other-more").bind('click', function (event) {
                event.stopPropagation();
                if ($("#other-more").html().indexOf('更多') > -1) {
                    $(this).html("收起<i class=\"iconfont\">&#xe917;</i>");
                    $(".other-list ul").show();
                } else {
                    $(this).html("更多<i class=\"iconfont\">&#xe916;</i>");
                    $(".other-list ul").hide();
                }
                $("#orther-ul-1").show();
            });
            //车龄 
            Age.init("[[\"1年内\",\"/china/a0_1msdgscncgpiltocspexx0/#pvareaid=100513\",0],[\"1-3年\",\"/china/a1_3msdgscncgpiltocspexx0/#pvareaid=100513\",0],[\"3-5年\",\"/china/a3_5msdgscncgpiltocspexx0/#pvareaid=100513\",0],[\"5-8年\",\"/china/a5_8msdgscncgpiltocspexx0/#pvareaid=100513\",0],[\"8-10年\",\"/china/a8_10msdgscncgpiltocspexx0/#pvareaid=100513\",0],[\"10年以上\",\"/china/a10_0msdgscncgpiltocspexx0/#pvareaid=100513\",0]]");
            CarClass.init("[[\"微型车\",\"a00\",\"-\",0],[\"小型车\",\"a0\",\"-\",0],[\"紧凑型\",\"a\",\"-\",0],[\"中型车\",\"b\",\"-\",0],[\"中大型\",\"c\",\"-\",0],[\"大型车\",\"d\",\"-\",0],[\"MPV\",\"mpv\",\"-\",0],[\"全部SUV\",\"suv\",\"/china/suv/\",0],[\"小型SUV\",\"suva0\",\"-\",0],[\"紧凑型SUV\",\"suva\",\"-\",0],[\"中型SUV\",\"suvb\",\"-\",0],[\"中大型SUV\",\"suvc\",\"-\",0],[\"大型SUV\",\"suvd\",\"-\",0],[\"跑车\",\"s\",\"-\",0],[\"微面\",\"mb\",\"-\",0],[\"皮卡\",\"p\",\"-\",0]]");
            Displace.init("[[\"1.0L及以下\",\"-\",\"1\",\"0\"],[\"1.1L-1.6L\",\"-\",\"2\",\"0\"],[\"1.7L-2.0L\",\"-\",\"3\",\"0\"],[\"2.1L-2.5L\",\"-\",\"4\",\"0\"],[\"2.6L-3.0L\",\"-\",\"5\",\"0\"],[\"3.1L-4.0L\",\"-\",\"6\",\"0\"],[\"4.0L以上\",\"-\",\"7\",\"0\"]]");
            Mileage.init("[[\"1万公里内\",\"/china/a0_0m1sdgscncgpiltocspexx0/#pvareaid=100514\",1,0],[\"3万公里内\",\"/china/a0_0m3sdgscncgpiltocspexx0/#pvareaid=100514\",3,0],[\"6万公里内\",\"/china/a0_0m6sdgscncgpiltocspexx0/#pvareaid=100514\",6,0],[\"10万公里内\",\"/china/a0_0m10sdgscncgpiltocspexx0/#pvareaid=100514\",10,0]]");
            Gearbox.init("[[\"手动\",\"/china/a0_0msdg1scncgpiltocspexx0/#pvareaid=100516\",0],[\"自动\",\"/china/a0_0msdg2scncgpiltocspexx0/#pvareaid=100516\",0]]");
            Power.init("[[\"前驱\",\"-\",1,0],[\"后驱\",\"-\",2,0],[\"四驱\",\"-\",3,0]]");
            Protect.init("[[\"合资\",\"-\",1,0],[\"中国\",\"-\",2,0],[\"进口\",\"-\",3,0]]");
            Struct.init("[[\"两厢\",\"-\",1,0],[\"三厢\",\"-\",2,0],[\"掀背\",\"-\",3,0],[\"旅行版\",\"-\",4,0]]");
            Nation.init("[[\"中国\",\"-\",\"1\",0],[\"德国\",\"-\",\"2\",0],[\"日本\",\"-\",3,0],[\"美国\",\"-\",4,0],[\"韩国\",\"-\",5,0],[\"法国\",\"-\",6,\"0\"],[\"英国\",\"-\",7,0],[\"意大利\",\"-\",8,0],[\"瑞典\",\"-\",9,0],[\"荷兰\",null,\"10\",\"0\"],[\"捷克\",\"-\",\"11\",0],[\"西班牙\",\"-\",12,0],[\"奥地利\",\"-\",13,0],[\"俄罗斯\",null,14,0],[\"丹麦\",null,15,0],[\"罗马尼亚\",null,16,0]]");
            Color.init("[[\"黑色\",\"-\",\"#000000\",1,0],[\"白色\",\"-\",\"#FFFFFF\",2,0],[\"银灰色\",\"-\",\"#bec3c4\",3,0],[\"深灰色\",\"-\",\"#595959\",4,0],[\"红色\",\"-\",\"#e60012\",5,0],[\"蓝色\",\"-\",\"#00a1e9\",6,0],[\"绿色\",\"-\",\"#009944\",7,0],[\"黄色\",\"-\",\"#fff33f\",8,0],[\"香槟色\",\"-\",\"#cd7f32\",9,0],[\"紫色\",\"-\",\"#4f2f4f\",10,0],[\"橙色\",\"-\",\"#ff0000\",12,0],[\"棕色\",\"-\",\"#482807\",13,0],[\"其他\",\"-\",\"\",11,0]]");
            Fule.init("[[\"汽油\",\"-\",1,0],[\"柴油\",\"-\",2,0],[\"油电混合\",\"-\",3,0],[\"电动\",\"-\",4,0]]");
            Seat.init("[[\"2座\",\"-\",\"2\",0],[\"4座\",\"-\",4,0],[\"5座\",\"-\",5,0],[\"6座\",\"-\",6,0],[\"7座及以上\",\"-\",10,0]]");
            Option.init("[[\"电动吸合门\",\"电动吸合门\",\"-\",\"19\",0],[\"空气悬挂\",\"空气悬挂\",\"-\",9,0],[\"氙气大灯\",\"氙气大灯\",\"-\",\"17\",0],[\"倒车视频影像\",\"倒车视频影\",\"-\",\"20\",0],[\"车身稳定控制(ESC/ESP/DSC等)\",\"车身稳定控\",\"-\",8,0],[\"GPS导航系统\",\"GPS导航系统\",\"-\",16,0],[\"前排座椅加热\",\"前排座椅加\",\"-\",21,0],[\"胎压监测装置\",\"胎压监测装\",\"-\",1,0],[\"无钥匙启动系统\",\"无钥匙启动\",\"-\",6,0],[\"自动空调\",\"自动空调\",\"-\",18,0],[\"定速巡航\",\"定速巡航\",\"-\",13,0],[\"多功能方向盘\",\"多功能方向\",\"-\",12,0],[\"电动天窗\",\"电动天窗\",\"-\",10,0],[\"真皮/仿皮座椅\",\"真皮/仿皮座\",\"-\",15,0],[\"真皮方向盘\",\"真皮方向盘\",\"-\",11,0],[\"泊车辅助\",\"泊车辅助\",\"-\",14,0],[\"遥控钥匙\",\"遥控钥匙\",\"-\",5,0],[\"车内中控锁\",\"车内中控锁\",\"-\",4,0],[\"发动机电子防盗\",\"发动机电子\",\"-\",3,0],[\"ISOFIX儿童座椅接口\",\"ISOFIX儿童\",\"-\",2,0],[\"ABS防抱死\",\"ABS防抱死\",\"-\",7,0]]");
            Environmental.init("[[\"国II\",\"-\",2,0],[\"国III\",\"-\",3,0],[\"国IV\",\"-\",4,0],[\"国V\",\"-\",5,0],[\"欧II\",\"-\",102,0],[\"欧III\",\"-\",103,0],[\"欧IV\",\"-\",104,0],[\"欧V\",\"-\",105,0],[\"欧VI\",\"-\",106,0]]");
        } else {
            if (!FilterOther.Isload) {
                var filterInfo = $("#filterInfo").val();
                var requestUrl = '/handler/UsedCarListControl.ashx?action=all&' + filterInfo + '&kw=' + $("#filterKw").val() + '&v=2016121215160102';
                var _this = this;
                $.ajax({
                    url: requestUrl,
                    success: function (data) {
                        data = eval(data);
                        FilterOther.Isload = true;
                        var uli = $("#ulother li");
                        var temdata = new Array();
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].ischeck) {
                                temdata.push(i);
                            }
                        }
                        if (temdata.length == 0) {
                            $("#other-div").hide(); return;
                        }
                        var stringBuder = "";
                        if (temdata.length > 6) {
                            var li = uli.eq(temdata[5]);
                            var div = li.find(".select-option");
                            if (div) {
                                div.attr("style", "left: inherit; right: 0px; ");
                            }
                            if (temdata.length >= 12) {
                                var li = uli.eq(temdata[11]);
                                var div = li.find(".select-option");
                                if (div) {
                                    div.attr("style", "left: inherit; right: 0px; ");
                                }
                                if (temdata.length > 12) {
                                    var li = uli.eq(temdata[12]);
                                    div = li.find(".select-option");
                                    if (div) {
                                        div.attr("style", "left: inherit; right: 0px; ");
                                    }
                                }
                            }
                            stringBuder += '<ul class=\"condition-con fn-clear\" id=\"orther-ul-1\">';
                            for (var i = 0; i < 6; i++) {
                                stringBuder += '<li>' + uli.eq(temdata[i]).html() + '</li>';
                            }
                            stringBuder += '<a href=\"javascript:void(0)\" id=\"other-more\" class=\"more\">更多<i class=\"iconfont\">&#xe916;</i></a></ul>';
                            stringBuder += '<ul class=\"condition-con fn-clear fn-hide\">';
                            var temi = temdata.length;
                            for (var i = 6; i < temi; i++) {
                                stringBuder += '<li>' + uli.eq(temdata[i]).html() + '</li>';
                            }
                            stringBuder += '</ul>';

                            $(".other-list").html(stringBuder);
                            $("#other-more").bind('click', function (event) {
                                event.stopPropagation();
                                if ($("#other-more").html().indexOf('更多') > -1) {
                                    $(this).html("收起<i class=\"iconfont\">&#xe917;</i>");
                                    $(".other-list ul").show();
                                } else {
                                    $(this).html("更多<i class=\"iconfont\">&#xe916;</i>");
                                    $(".other-list ul").hide();
                                }
                                $("#orther-ul-1").show();
                            });
                        } else if (temdata.length == 6) {
                            var li = uli.eq(temdata[5]);
                            var div = li.find(".select-option");
                            if (div) {
                                div.attr("style", "left: inherit; right: 0px; ");
                            }
                        }
                        if (!data[0].ischeck) {
                            Age.Hide();
                        } else {
                            Age.init(data[0].value);
                        }
                        if (!data[1].ischeck) {
                            CarClass.Hide();
                        } else {
                            CarClass.init(data[1].value);
                        }
                        if (!data[2].ischeck) {
                            Displace.Hide();
                        } else {
                            Displace.init(data[2].value);
                        }
                        if (!data[3].ischeck) {
                            Environmental.Hide();
                        } else {
                            Environmental.init(data[3].value);
                        }
                        if (!data[4].ischeck) {
                            Mileage.Hide();
                        } else {
                            Mileage.init(data[4].value);
                        }
                        if (!data[5].ischeck) {
                            Gearbox.Hide();
                        } else {
                            Gearbox.init(data[5].value);
                        }
                        if (!data[6].ischeck) {
                            Power.Hide();
                        } else {
                            Power.init(data[6].value);
                        }
                        if (!data[7].ischeck) {
                            Protect.Hide();
                        } else {
                            Protect.init(data[7].value);
                        }
                        if (!data[8].ischeck) {
                            Struct.Hide();
                        } else {
                            Struct.init(data[8].value);
                        }
                        if (!data[9].ischeck) {
                            Color.Hide();
                        } else {
                            Color.init(data[9].value);
                        }
                        if (!data[10].ischeck) {
                            Nation.Hide();
                        } else {
                            Nation.init(data[10].value);
                        }
                        if (!data[11].ischeck) {
                            Option.Hide();
                        } else {
                            Option.init(data[11].value);
                        } if (!data[12].ischeck) {
                            Fule.Hide();
                        } else {
                            Fule.init(data[12].value);
                        }
                        if (!data[13].ischeck) {
                            Seat.Hide();
                        } else {
                            Seat.init(data[13].value);
                        }

                    }
                });
            }
        }
    }

    //金牌店铺
    var GetGoldDealers = new Object();
    GetGoldDealers.init = function () {
        var filterInfo = $("#filterInfo").val();
        var _filterval = $("#filterInfoJs").val();
        var solrfilterinfo = SolrFilterInfo.Convert(_filterval);
        if (solrfilterinfo.BrandPinyin != '' || solrfilterinfo.DealerId > 0) {
            var requestUrl = '/handler/carlist/GetGoldDealers.ashx?' + filterInfo + '&v=201703311821';
            var _this = this;
            $.ajax({
                url: requestUrl,
                success: function (data) {
                    if (data != 'NORESULT') {
                        var reslut = data.split('$$');
                        $("#flagship-top").html(reslut[0]);
                        // 金牌店铺埋点
                        var _dataSubmit = new Object();
                        _dataSubmit.eventkey = "s_pc_carlist_dealercard";
                        _dataSubmit.info = escape(getCookie('userarea') + '^' + reslut[1]);
                        _dataSubmit.ref = escape(document.referrer);
                        _dataSubmit.cur = escape(location.href);
                        _dataSubmit.rm = new Date().getTime();

                        $.ajax({
                            url: "//collectionpv.che168.com/collect/page_event.ashx",
                            type: "get",
                            dataType: "jsonp",
                            data: _dataSubmit

                        });
                    }
                }
            });
        }
    };

    var FilterConditions = new Object();
    FilterConditions.init = function () {
        var CurConditionsLis = $("#CurConditions li");
        var FilterNoResult = $("#FilterNoResult");
        var html = '<li class=\"list-info\">抱歉，没有找到</li>';
        html += $("#CurConditions").html();
        FilterNoResult.html(html);

        if (CurConditionsLis.length > 8) {
            var lis8 = $("#FilterNoResult li:gt(7)");
            lis8.hide();
            FilterNoResult.append('<li id=\"filter_listmore\" class=\"list-more\"><span>展开全部</span><a href=\"javascript:void(0)\" class=\"iconfont\">&#xe916;</a></li><li class=\"list-info\">的车源</li>');
            $("#filter_listmore").on("click", function () {
                lis8.show();
                $("#filter_listmore").hide();
            });
        } else {
            FilterNoResult.append('<li class=\"list-info\">的车源</li>');
        }
    };

    var Page = new Object();
    Page.init = function () {
        this.filterval = $("#filterInfoJs").val();
        var _this = this;
        this.page = $("#pagenumber");
        this.totlenumber = $("#pagetotalnumber");
        this.btn = $("#pagego");
        this.page.on("keydown", function (event) {
            if ((event.which >= 48 && event.which <= 59) || (event.which >= 96 && event.which <= 105) || event.which == 8 || event.which == 9 || event.which == 37 || event.which == 39 || event.which == 46) {
            } else {
                event.preventDefault();
            }
        });
        this.btn.on("click", function () {
            if (Tool.GetNumber(_this.page.val()) <= Tool.GetNumber(_this.totlenumber.html()) && Tool.GetNumber(_this.page.val()) != 0) {

                var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
                solrfilterinfo.PageIdx = Tool.GetNumber(_this.page.val());
                window.location = solrfilterinfo.BuilderUrl();
            }
        });
    };
    var Hot = new Object();
    Hot.init = function () {
        this.more = $("#hotmore");
        this.pop = $("#hotpop");
        new TipClickQuery(this.more, this.pop, "", "", function (more, pop) {
            more.html("收起<i class=\"iconfont\">&#xe917;</i>"); pop.removeClass("fn-hide");
        }, function (more, pop) { pop.addClass("fn-hide"); more.html("更多<i class=\"iconfont\">&#xe916;</i>"); });
    };
    var Brand = new Object();
    Brand.init = function () {
        this.pop = $("#brandshow");
        this.b_brandtaggletimeid = 0;
        this.more = $("#brandmore");

        var _this = this;
        this.isload = false;
        this.more.on("mouseover", function () {
            if (!_this.isload) {
                _this.isload = true;
                _this.loadCarBrand();
            }
        });
        new TipClickQuery(this.more, this.pop, "", "", function (more, pop) {
            more.html("收起<i class=\"iconfont\">&#xe917;</i>"); pop.removeClass("fn-hide");
        }, function (more, pop) { pop.addClass("fn-hide"); more.html("更多<i class=\"iconfont\">&#xe916;</i>"); });

    };


    Brand.brandChange = function (obj) {
        var _this = $(obj);
        this.b_brandtaggletimeid = setTimeout(function () {
            clearTimeout(this.b_brandtaggletimeid);
            var brandcon = $("#brand" + _this.attr('data-value'));
            $(".brand2-city").hide();
            brandcon.show()
            _this.addClass("current").siblings().removeClass();
        }, 40);
    }
    Brand.ClearTime = function () {
        clearTimeout(this.b_brandtaggletimeid);
    }
    Brand.loadCarBrand = function () {
        var filterInfo = $("#filterInfo").val();
        var requestUrl = '/handler/usedcarlistv5.ashx?action=brandlist&' + filterInfo + '';
        var _this = this;
        $.getJSON(requestUrl, function (data) {
            if (data && data.length > 0) {
                var temp = data[0].letters;
                var htmlA = ["<a class='current'  data-value=\"" + temp + "\" href=\"javascript:void(0)\">" + temp + "</a>"];
                var htmlB = ["<div id=\"brand" + temp + "\" class=\"brand2-city\"></div>"];
                var htmlC = [];
                $(data).each(function (i, item) {
                    if (temp != item.letters) {
                        temp = item.letters;
                        htmlA.push('<a  data-value="' + temp + '" href=\"javascript:void(0)\">' + temp + '</a>');
                        htmlB.push('<div id="brand' + temp + '" style="display:none;" class="brand2-city"></div>');

                    }
                });
                $("#brandAZ").html(htmlA.join(''));//加载外层A-Z存在的选项卡
                $("#brandAZ").find("a").each(function () {
                    $(this).on("mouseover", function () {
                        _this.brandChange(this);
                    });
                    $(this).on("mouseout", function () {
                        _this.ClearTime()
                    });
                });
                $("#brandAZ").after(htmlB.join(''));//加载内容层外层A-Z      
                var temp = data[0].letters;
                $(data).each(function (i, item) {
                    if (temp != item.letters) {
                        $("#brand" + temp).html(htmlC.join('')); htmlC = [];
                        temp = item.letters;
                    }
                    htmlC.push("<dl><dd><a href='" + item.url + "#pvareaid=100511'>" + item.name + "</a></dd></dl>");

                });
                $("#brand" + temp).html(htmlC.join(''));
            }
        });

    }
    var Series = new Object();
    Series.init = function () {
        var _this = this;
        this.filterval = $("#filterInfoJs").val();
        this.more = $("#href_series_more");
        this.pop = $("#seriespop");
        this.contain = $("#series");
        this.hotserices = $("#hotserices");
        this.hotseriesmore = $("#hotseriesmore");
        this.href_series_close = $("#href_series_close");

        this.btnsurce = $("#btnseriessure");
        this.btncancle = $("#btnseriescancle");
        this.hotseriesmore.on("click", function (event) {
            event.stopPropagation();

            _this.contain.removeClass("fn-hide");
            _this.hotserices.addClass("fn-hide");
            $(".condition").css({ "position": "relative" });
        });
        this.href_series_close.on("click", function () {
            event.stopPropagation();
            _this.contain.addClass("fn-hide");
            _this.hotserices.removeClass("fn-hide");
            $(".condition").css({ "position": "inherit" });

        });
        $("#href_series_close1").on("click", function () {
            event.stopPropagation();
            _this.pop.addClass("fn-hide");
            _this.hotserices.removeClass("fn-hide");
            $(".condition").css({ "position": "inherit" });
        });
        this.more.on("click", function () {

            if (_this.pop.hasClass("fn-hide")) {
                stopEvent();//取消事件冒泡
                $(".condition").css({ "position": "relative" });
                _this.pop.removeClass("fn-hide");
                _this.contain.addClass("fn-hide");
                _this.hotserices.addClass("fn-hide");
                _this.pop.find("[checked=checked]").prop("checked", true);
                if (_this.pop.find(":checked").size() > 0) {
                    _this.btnsurce.removeClass("confirm-disable").addClass("confirm-blue");
                }
                if (_this.pop.find(":checked").size() >= 3) {
                    _this.pop.find("input[type=checkbox]:not(:checked)").prop("disabled", true);
                }
                else {
                    _this.pop.find("input[type=checkbox]").prop("disabled", false);
                }
            }
        });
        this.pop.find("input[type=checkbox]").on("click", function () {

            if (_this.pop.find(":checked").size() >= 3) {
                _this.pop.find("input[type=checkbox]:not(:checked)").prop("disabled", true);
            }
            else {
                _this.pop.find("input[type=checkbox]").prop("disabled", false);
            }
            if (_this.pop.find(":checked").size() > 0) {
                _this.btnsurce.removeClass("confirm-disable").addClass("confirm-blue");
            }
            else {
                _this.btnsurce.removeClass("confirm-blue").addClass("confirm-disable");
            }
        }
            );
        this.btnsurce.on("click", function () {
            var seriesid = [];
            var seriespinyin = [];
            _this.pop.find(":checked").each(function () {
                seriesid.push(Tool.GetNumber($(this).attr("seriesid")));
                seriespinyin.push($(this).attr("pinyin"));
            });
            if (seriesid > 3)
                return;
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.SeriesPinyin = seriespinyin.join('-');
            solrfilterinfo.SeriesId = seriesid;
            window.location = solrfilterinfo.BuilderUrl(100512);
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false);
            _this.pop.addClass("fn-hide");
            _this.hotserices.removeClass("fn-hide");
        });
    }

    var SeriesYear = new Object()
    SeriesYear.init = function () {

    }

    var Spec = new Object();
    Spec.init = function () {
        var _this = this;
        this.filterval = $("#filterInfoJs").val();
        this.more = $(".moreseriesyear");
        this.pop = $(".seriesyearpop");
        this.btnsure = $(".seriesyearsure");
        this.btncancle = $(".seriesyearcancle");

        this.more.each(function () {
            var _pop = new Object();
            var _more = $(this);
            var moreyearid = Tool.GetNumber(_more.attr("yearid"));

            _this.pop.each(function (index, ele) {
                var yearid = Tool.GetNumber($(this).attr("yearid"))
                if (moreyearid == yearid) {
                    _pop = $(this);
                    if ((index > 4 && index < 7) || (index > 11 && index < 14) || (index > 18 && index < 21)) {
                        _pop.css("left", "inherit").css("right", "0px");
                    }
                    if (_pop.find(":checked").size() > 0) {
                        _more.css("border", "solid 1px #4680d1");
                        _more.children("span").css("color", "#4680d1");
                        _more.children("i").css("color", "#4680d1");
                        var len = $("#seriesyear" + yearid + "pop").find("input[all = 0]").length;
                        if ($("#seriesyear" + yearid + "pop").find("input[all=0]:checked").size() == len) {
                            $("#seriesyear" + yearid + "pop").find("input[all=1]").prop("checked", "checked");
                        }

                    }
                }
            });
            new TipClickQuery(_more, _pop, "", "", function (_more, _pop) {
                _more.parent().addClass("active"); _pop.removeClass("fn-hide"); _more.parents("div.condition-list").css("z-index", "202");
            }, function (_more, _pop) { _more.parent().removeClass("active"); _pop.addClass("fn-hide"); });
        });

        this.pop.find("input[type=checkbox]").on("click", function (event) {
            event.stopPropagation();//取消事件冒泡；
            var yearid = $(this).attr("yearid");
            var len = $("#seriesyear" + yearid + "pop").find("input[all=0]").length;
            if ($(this).attr("all") == "1") {
                $("#seriesyear" + yearid + "pop").find("input[type=checkbox]").prop("checked", $(this).prop('checked'));
            } else {
                var allcheck = $("#seriesyear" + yearid + "pop").find("input[all=1]");
                if ($("#seriesyear" + yearid + "pop").find("input[all=0]:checked").size() == len) {
                    allcheck.prop("checked", "checked");
                } else {
                    allcheck.prop("checked", "");
                }

            }
        }
          );
        this.btnsure.on("click", function () {
            var yearid = Tool.GetNumber($(this).attr("yearid"));
            var input = null, input1;
            input = $(".seriesyearpop").find("input:checked");
            var specid = []
            var seriesyearid = [];
            if (input != null) {
                input.each(function () {
                    if ($(this).attr("all") != "1") {
                        specid.push($(this).attr("specid"));
                        seriesyearid.push($(this).attr("yearid"));
                    }
                });
            }
            var n = {}, r = []; //n为hash表，r为临时数组
            for (var i = 0; i < seriesyearid.length; i++) //遍历当前数组
            {
                if (!n[seriesyearid[i]]) //如果hash表中没有当前项
                {
                    n[seriesyearid[i]] = true; //存入hash表
                    r.push(seriesyearid[i]); //把当前数组的当前项push到临时数组里面
                }
            }


            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.SpecId = specid;
            solrfilterinfo.SeriesYearId = r;
            window.location = solrfilterinfo.BuilderUrl(102132);
        });
        this.btncancle.on("click", function () {
            var yearid = $(this).attr("yearid");
            $("#seriesyear" + yearid + "pop").hide().find(":checked").prop("checked", false);
            $("#seriesyear" + yearid + "more").removeClass("active");
        });
    }
    var Price = new Object();
    Price.init = function () {
        var _this = this;
        this.filterval = $("#filterInfoJs").val();
        this.custompricesurce = $("#custompricesurce");
        this.custommaxprice = $("#custommaxprice");
        this.customminprice = $("#customminprice");
        this.custommaxprice.on("keyup", function () {
            if (!_this.vail()) {
                _this.custompricesurce.removeClass("confirm-blue").addClass("confirm-disable");
            }
            else {
                _this.custompricesurce.removeClass("confirm-disable").addClass("confirm-blue");
            }
        });
        this.customminprice.on("keyup", function () {
            if (!_this.vail()) {
                _this.custompricesurce.removeClass("confirm-blue").addClass("confirm-disable");
            }
            else {
                _this.custompricesurce.removeClass("confirm-disable").addClass("confirm-blue");
            }
        });
        this.custompricesurce.on("click", function () {
            if (_this.vail()) {
                var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
                solrfilterinfo.MaxPrice = Tool.GetNumber(_this.custommaxprice.val());
                solrfilterinfo.MinPrice = Tool.GetNumber(_this.customminprice.val());
                window.location = solrfilterinfo.BuilderUrl(100508);
            }
        })
    }
    Price.vail = function () {
        if (!Tool.IsNumber(this.custommaxprice.val()) || !Tool.IsNumber(this.customminprice.val())) {
            return false;
        }
        var minprice = Tool.GetNumber(this.customminprice.val());
        var maxprice = Tool.GetNumber(this.custommaxprice.val());
        if (minprice > 10000 || maxprice > 10000)
            return false;
        if (maxprice < minprice)
            return false;
        if (minprice == maxprice && maxprice == 0)
            return false;
        return true;
    }

    var Age = new Object();
    Age.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.pop = $("#radiopop");
        this.more = $("#agemore");

        this.customage = $("#customage");
        this.custommaxage = $("#custommaxage");
        this.customminage = $("#customminage");
        this.customsurce = $("#customagesurce");
        _this.loadCarAge(data);

        this.custommaxage.on("keyup", function () {
            if (!_this.vail()) {
                _this.customsurce.removeClass("confirm-blue").addClass("confirm-disable");
            }
            else {
                _this.customsurce.removeClass("confirm-disable").addClass("confirm-blue");
            }
        });
        this.customminage.on("keyup", function () {
            if (!_this.vail()) {
                _this.customsurce.removeClass("confirm-blue").addClass("confirm-disable");
            }
            else {
                _this.customsurce.removeClass("confirm-disable").addClass("confirm-blue");
            }
        });

        this.customsurce.on("click", function () {
            if (_this.vail()) {
                var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
                solrfilterinfo.MaxAge = Tool.GetNumber(_this.custommaxage.val());
                solrfilterinfo.MinAge = Tool.GetNumber(_this.customminage.val());
                window.location = solrfilterinfo.BuilderUrl(100513);
            }
        });

    }
    Age.vail = function () {
        if (!Tool.IsNumber(this.custommaxage.val()) || !Tool.IsNumber(this.customminage.val())) {
            return false;
        }

        var maxage = Tool.GetNumber(this.custommaxage.val());
        var minage = Tool.GetNumber(this.customminage.val());
        if (maxage > 100 || minage > 100)
            return false;
        if (maxage < minage)
            return false;
        if (maxage == minage && maxage == 0)
            return false;
        return true;
    }
    Age.loadCarAge = function (data) {
        data = eval(data);
        var _this = this;
        var sb = new StringBuffer();
        var num = 0;
        for (var i = 0; i < data.length; i++) {
            sb.AppendFormat("<label class='{0}'><a  class=\"con-mini\" href='{1}'>{2}</a></label>", Tool.GetNumber(data[i][2]) == 1 ? "active" : "", data[i][1], data[i][0]);
            num++;
        }
        if (num > 1) {
            new TipClickQuery(_this.more, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.customage.before(sb.ToString());
        } else {
            _this.more.parents("li").hide();
        }
    }
    Age.Hide = function () {
        $("#agemore").parents("li").hide();
    }

    var CarClass = new Object();
    CarClass.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.morclass = $("#moreclass");
        this.pop = $("#multiplepop");
        this.btnclassgroup = $("#btnclassgroup");
        this.surce = $("#btnclasssure");
        this.cancle = $("#btnclasscancle");
        _this.loadCarClass(data);
        var data = eval(data);
        this.datalen = data.length;
        this.surce.on("click", function () {
            var classpinyin = []
            _this.pop.find(":checked").each(function () {
                var suvpinyin = $(this).attr("pinyin");
                if (suvpinyin != "suv") {//排除掉全部suv选项
                    classpinyin.push(suvpinyin);
                }
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.ClassPinyin = classpinyin.join('-');

            var url = solrfilterinfo.BuilderUrl(100510);

            window.location.href = url;
        });
        this.cancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false);
            _this.pop.find(":checked").prop("disabled", false);
            _this.pop.addClass("fn-hide");
            _this.pop.hide();
            _this.morclass.parent().removeClass("active");

        });
    }
    CarClass.Hide = function () {
        $("#moreclass").parents("li").hide();
    }
    CarClass.loadCarClass = function (data) {
        var _this = this;
        //[0]名称 [1]拼音 [2]url [3]选中
        data = eval(data);
        var sb = new StringBuffer();
        var num = 0;
        var checked = false;
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][2])) {
                var issuv = false;
                var pinyin = data[i][1];
                if (pinyin.indexOf("suv") >= 0) {
                    if (pinyin != "suv") {
                        issuv = true;
                    }
                }
                sb.AppendFormat("<label><input type='checkbox' issuv='{3}' pinyin='{1}' {2} />{0}</label>", data[i][0], pinyin, Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "", issuv == true ? "1" : "0");
                num++;
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
            }
        }
        if (num > 1) {
            if (checked) {
                _this.morclass.css("border", "solid 1px #4680d1");
                _this.morclass.children("span").css("color", "#4680d1");
                _this.morclass.children("i").css("color", "#4680d1");
            }
            new TipClickQuery(_this.morclass, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btnclassgroup.html(sb.ToString());

            _this.pop.find("input[type=checkbox]").on("click", function () {
                //选择suv，则全部suv选中 
                if ($(this).attr("pinyin") == "suv") {
                    _this.pop.find("input[issuv=1]").prop("checked", $(this).prop('checked'));
                }
                if ($(this).attr("issuv") == "1") {
                    var leng = _this.pop.find("input[issuv=1]").length;
                    if (leng == _this.pop.find("input[issuv=1]:checked").length) {
                        _this.pop.find("input[pinyin=suv]").prop("checked", "checked");
                    } else {
                        _this.pop.find("input[pinyin=suv]").prop("checked", "");
                    }
                }

            });

        } else {
            _this.morclass.parents("li").hide();
        }
    }


    var Displace = new Object();
    Displace.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.pop = $("#multiplemoredisplacepop");
        this.btngroup = $("#btndisplacegroup");
        this.btnsurce = $("#btndisplacesurce");
        this.btncancle = $("#btndisplacecancle");
        this.moredisplace = $("#moredisplace");
        _this.loadCarDisplace(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsurce.on("click", function () {

            var displaceid = [];
            _this.pop.find(":checked").each(function () {
                displaceid.push(Tool.GetNumber($(this).attr("displaceid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.DisplacementId = displaceid;
            var url = solrfilterinfo.BuilderUrl(100515);
            window.location = url;
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false);
            _this.pop.find(":checked").prop("disabled", false);
            _this.pop.addClass("fn-hide");
            _this.pop.hide(); _this.moredisplace.parent().removeClass("active");

        })
    }
    Displace.Hide = function () {
        $("#moredisplace").parents("li").hide();
    }
    Displace.loadCarDisplace = function (data) {
        var _this = this;
        //0]名称 [1]url [2]displaceId [3]是否选中
        data = eval(data);
        var sb = new StringBuffer();
        var num = 0;
        var checked = false;
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                sb.AppendFormat("<label><input type='checkbox' displaceid='{1}' {2}>{0}</label>", data[i][0], data[i][2], Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "");
                num++;
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
            }
        }
        if (num > 1) {
            if (checked) {
                _this.moredisplace.css("border", "solid 1px #4680d1");
                _this.moredisplace.children("span").css("color", "#4680d1");
                _this.moredisplace.children("i").css("color", "#4680d1");
            }
            new TipClickQuery(_this.moredisplace, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());

        } else {
            _this.moredisplace.parents("li").hide();
        }

    }

    var Environmental = new Object();
    Environmental.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.pop = $("#multiplemoredenv");
        this.btngroup = $("#btnenvgroup");
        this.btnsurce = $("#btnenvsurce");
        this.btncancle = $("#btnenvcancle");
        this.more = $("#moreenv");
        _this.loadCarEnv(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsurce.on("click", function () {
            var displaceid = [];
            _this.pop.find(":checked").each(function () {
                displaceid.push(Tool.GetNumber($(this).attr("environmentalid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.Environmental = displaceid;
            var url = solrfilterinfo.BuilderUrl(100515);
            window.location = url;
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false);
            _this.pop.find(":checked").prop("disabled", false);
            _this.pop.addClass("fn-hide");
            _this.pop.hide(); _this.more.parent().removeClass("active");

        })
    }
    Environmental.Hide = function () {
        $("#moreenv").parents("li").hide();
    }
    Environmental.loadCarEnv = function (data) {
        var _this = this;
        //0]名称 [1]url [2]envid [3]是否选中
        data = eval(data);
        var sb = new StringBuffer();
        var num = 0;
        var checked = false;
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                sb.AppendFormat("<label><input type='checkbox' environmentalid='{1}' {2}>{0}</label>", data[i][0], data[i][2], Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "");
                num++;
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
            }
        }
        if (num > 1) {
            if (checked) {
                _this.more.css("border", "solid 1px #4680d1");
                _this.more.children("span").css("color", "#4680d1");
                _this.more.children("i").css("color", "#4680d1");
            }
            new TipClickQuery(_this.more, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());

        } else {
            _this.more.parents("li").hide();
        }

    }
    var Mileage = new Object();
    Mileage.init = function (data) {
        var _this = this;
        this.filterval = $("#filterInfoJs").val();
        this.isload = false;
        this.pop = $("#radiomileagepop");
        this.btngroup = $("#custommileage");
        this.btnsurce = $("#custommileagesurce");
        this.maxmileage = $("#custommaxmileage");
        this.minmileage = $("#customminmileage");
        this.moremileage = $("#moremileage");
        _this.loadCarMileage(data);

        this.minmileage.on("keyup", function () {
            if (!_this.vail()) {
                _this.btnsurce.removeClass("confirm-blue").addClass("confirm-disable");
            }
            else {
                _this.btnsurce.removeClass("confirm-disable").addClass("confirm-blue");
            }
        });
        this.maxmileage.on("keyup", function () {
            if (!_this.vail()) {
                _this.btnsurce.removeClass("confirm-blue").addClass("confirm-disable");
            }
            else {
                _this.btnsurce.removeClass("confirm-disable").addClass("confirm-blue");
            }
        });
        this.btnsurce.on("click", function () {
            if (_this.vail()) {
                var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
                solrfilterinfo.MaxMileage = Tool.GetNumber(_this.maxmileage.val());
                solrfilterinfo.MinMileage = Tool.GetNumber(_this.minmileage.val());
                var url = solrfilterinfo.BuilderUrl(100514);
                window.location = url;
            }
        });

    }

    Mileage.vail = function () {
        if (!Tool.IsNumber(this.maxmileage.val()) || !Tool.IsNumber(this.minmileage.val())) {
            return false;
        }
        var maxmileage = Tool.GetNumber(this.maxmileage.val());
        var minmilage = Tool.GetNumber(this.minmileage.val());
        if (maxmileage > 999 || minmilage > 999)
            return false;
        if (maxmileage < minmilage)
            return false;
        if (maxmileage == minmilage && maxmileage == 0)
            return false;
        return true;
    }
    Mileage.Hide = function () {
        $("#moremileage").parents("li").hide();
    }
    Mileage.loadCarMileage = function (data) {
        var _this = this;
        //[0]名称 [1]url [2]displaceId [3]是否选中
        data = eval(data);
        var num = 0;
        var sb = new StringBuffer();
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                sb.AppendFormat("<label class='{0}'><a class=\"con-mini\" href='{1}'>{2}</a></label>", Tool.GetNumber(data[i][3]) == 1 ? "active" : "", data[i][1], data[i][0]);
                num++;
            }
        }
        if (num > 1) {
            new TipClickQuery(_this.moremileage, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.before(sb.ToString());
        } else {
            _this.moremileage.parents("li").hide();
        }
    }

    var Gearbox = new Object();
    Gearbox.init = function (data) {
        var _this = this;
        this.filterval = $("filterInfo").val();
        this.isload = false;
        this.pop = $("#radiogearboxpop");
        this.moregearbox = $("#moregearbox");
        _this.loadCarGearbox(data);
    }
    Gearbox.Hide = function () {
        $("#moregearbox").parents("li").hide();
    }
    Gearbox.loadCarGearbox = function (data) {
        var _this = this;
        //[0]名称 [1]url [2]是否选中
        data = eval(data);
        var num = 0;
        var checked = false;
        var sb = new StringBuffer();
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                sb.AppendFormat("<label class='{0}'><a class=\"con-mini\" href='{1}'>{2}</a></label>", Tool.GetNumber(data[i][2]) == 1 ? "active" : "", data[i][1], data[i][0]);
                num++;
                if (Tool.GetNumber(data[i][2]) == 1) {
                    checked = true;
                }
            }
        }
        if (num > 1) {
            if (checked) {
                _this.moregearbox.css("border", "solid 1px #4680d1");
                _this.moregearbox.children("span").css("color", "#4680d1");
                _this.moregearbox.children("i").css("color", "#4680d1");
            }
            new TipClickQuery(_this.moregearbox, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.pop.append(sb.ToString());
        } else {
            _this.moregearbox.parents("li").hide();
        }

    }

    var Power = new Object();
    Power.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.morepower = $("#morepower");
        this.pop = $("#multipmorepower");
        this.btngroup = $("#btnpowergroup");
        this.btnsure = $("#btndpowersurce");
        this.btncancle = $("#btnpowercancle");
        _this.loadCarPower(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsure.on("click", function () {

            var powerid = [];
            _this.pop.find(":checked").each(function () {
                powerid.push(Tool.GetNumber($(this).attr("powerid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.PowerTrain = powerid;

            var url = solrfilterinfo.BuilderUrl(100517);
            window.location = url;
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false);
            _this.pop.find(":checked").prop("disabled", false);
            _this.pop.addClass("fn-hide");
            _this.pop.hide();
            _this.morepower.parent().removeClass("active");

        });
    }
    Power.Hide = function () {
        $("#morepower").parents("li").hide();
    }
    Power.loadCarPower = function (data) {
        var _this = this;
        //[0]名称[1]url[2]id[3]是否选中
        data = eval(data);
        var sb = new StringBuffer();
        var num = 0;
        var checked = false;
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                num++;
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
                sb.AppendFormat("<label><input type='checkbox' powerid='{1}' {2}>{0}</label>", data[i][0], data[i][2], Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "");
            }
        }
        if (num > 1) {
            if (checked) {
                _this.morepower.css("border", "solid 1px #4680d1");
                _this.morepower.children("span").css("color", "#4680d1");
                _this.morepower.children("i").css("color", "#4680d1");

            }
            new TipClickQuery(_this.morepower, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());
        } else {
            _this.morepower.parents("li").hide();
        }

    }
    //属性
    var Protect = new Object()
    Protect.init = function (data) {
        var _this = this;
        this.filterval = $("#filterInfoJs").val();
        this.isload = false;
        this.moreprotect = $("#moreprotect");
        this.btngroup = $("#btnprotectegroup");
        this.btnsure = $("#btnprotectsurce");
        this.btncancle = $("#btnprotectcancle");
        this.pop = $("#multipmoreprotect");
        _this.loadCarProtect(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsure.on("click", function () {

            var courtyid = [];
            _this.pop.find(":checked").each(function () {
                courtyid.push(Tool.GetNumber($(this).attr("courtyid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.CountryType = courtyid;

            var url = solrfilterinfo.BuilderUrl(101183);
            window.location = url;
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false).prop("disabled", false);
            _this.pop.addClass("fn-hide").hide();
            _this.moreprotect.parent().removeClass("active");

        })
    }
    Protect.Hide = function () {
        $("#moreprotect").parents("li").hide();
    }
    Protect.loadCarProtect = function (data) {
        var _this = this;
        //[0]名称 [1]url [2]id [3]是否选中
        data = eval(data);
        var num = 0;
        var checked = false;
        var sb = new StringBuffer();
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                sb.AppendFormat("<label><input type='checkbox' courtyid='{1}' {2}>{0}</label>", data[i][0], data[i][2], Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "");
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
                num++;
            }
        }
        if (num > 1) {
            if (checked) {
                _this.moreprotect.css("border", "solid 1px #4680d1");
                _this.moreprotect.children("span").css("color", "#4680d1");
                _this.moreprotect.children("i").css("color", "#4680d1");
                //_this.btnsure.removeClass("confirm-disable").addClass("confirm-blue");
            }
            new TipClickQuery(_this.moreprotect, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());

        } else {
            _this.moreprotect.parents("li").hide();
        }

    }

    var Struct = new Object();
    Struct.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.morestruct = $("#morsturct");
        this.btngroup = $("#btnstructgroup");
        this.btnsurce = $("#btnstructsurce");
        this.btncancle = $("#btnstructcancle");
        this.pop = $("#multipmorestruct");
        _this.loadCarStruct(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsurce.on("click", function () {

            var structid = [];
            _this.pop.find(":checked").each(function () {
                structid.push(Tool.GetNumber($(this).attr("structid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.Structure = structid;
            var url = solrfilterinfo.BuilderUrl(101184);

            window.location = url;
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false).prop("disabled", false);
            _this.pop.addClass("fn-hide").hide();
            _this.morestruct.parent().removeClass("active");

        })
    }
    Struct.Hide = function () {
        $("#morsturct").parents("li").hide();
    }
    Struct.loadCarStruct = function (data) {
        var _this = this;
        //[0]名称 [1]url [2]id [3]是否选中
        data = eval(data);
        var num = 0;
        var checked = false;
        var sb = new StringBuffer();
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                num++;
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
                sb.AppendFormat("<label><input type='checkbox' structid='{1}' {2}>{0}</label>", data[i][0], data[i][2], Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "");
            }
        }
        if (num > 1) {
            if (checked) {
                _this.morestruct.css("border", "solid 1px #4680d1");
                _this.morestruct.children("span").css("color", "#4680d1");
                _this.morestruct.children("i").css("color", "#4680d1");

            }
            new TipClickQuery(_this.morestruct, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide");
                more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());

        } else {
            _this.morestruct.parents("li").hide();
        }

    }

    var Color = new Object();
    Color.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.morecolor = $("#morecolor");
        this.btngroup = $("#btncolorgroup");
        this.pop = $("#multipmorecolor");
        this.btnsure = $("#btncolorsurce");
        this.btncancle = $("#btncolorcancle");
        _this.loadCarColor(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsure.on("click", function () {

            var colorid = [];
            _this.pop.find(":checked").each(function () {
                colorid.push(Tool.GetNumber($(this).attr("colorid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.Color = colorid;

            var url = solrfilterinfo.BuilderUrl(101185);
            window.location = url;
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false).prop("disabled", false);
            _this.pop.addClass("fn-hide").hide();
            _this.morecolor.parent().removeClass("active");

        })
    }
    Color.Hide = function () {
        $("#morecolor").parents("li").hide();
    }
    Color.loadCarColor = function (data) {
        var _this = this;
        //0]名称 [1]url [2]css [3]id
        data = eval(data);
        var num = 0;
        var checked = false;
        var sb = new StringBuffer();
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                num++;
                if (Tool.GetNumber(data[i][4]) == 1) {
                    checked = true;
                }
                sb.AppendFormat("<label><input type='checkbox' colorid='{1}' {2}>{0}</label>", data[i][0], data[i][3], Tool.GetNumber(data[i][4]) == 1 ? "checked='checked'" : "");
            }
        }
        if (num > 1) {
            if (checked) {
                _this.morecolor.css("border", "solid 1px #4680d1");
                _this.morecolor.children("span").css("color", "#4680d1");
                _this.morecolor.children("i").css("color", "#4680d1");

            }
            new TipClickQuery(_this.morecolor, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());
        } else {
            _this.morecolor.parents("li").hide();
        }

    }
    var Nation = new Object();
    Nation.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.morenation = $("#morenation");
        this.pop = $("#multipmorenation");
        this.btngroup = $("#btnnationgroup");
        this.btnsure = $("#btnnationsurce");
        this.btncancle = $("#btnnationcancle");
        _this.loadCarNation(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsure.on("click", function () {

            var nationid = [];
            _this.pop.find(":checked").each(function () {
                nationid.push(Tool.GetNumber($(this).attr("nationid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.Nationality = nationid;

            var url = solrfilterinfo.BuilderUrl(101177);
            window.location = url;
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false).prop("disabled", false);
            _this.pop.addClass("fn-hide").hide();

            _this.morenation.parent().removeClass("active");
        })
    }
    Nation.Hide = function () {
        $("#morenation").parents("li").hide();
    }
    Nation.loadCarNation = function (data) {
        var _this = this;
        //[0]名称[1]url[2]id [3]是否选中
        data = eval(data); var num = 0;
        var checked = false;
        var sb = new StringBuffer();
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                num++;
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
                sb.AppendFormat("<label><input type='checkbox' nationid='{1}' {2}>{0}</label>", $.trim(data[i][0]), data[i][2], Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "");
            }
        }
        if (num > 1) {
            if (checked) {
                _this.morenation.css("border", "solid 1px #4680d1");
                _this.morenation.children("span").css("color", "#4680d1");
                _this.morenation.children("i").css("color", "#4680d1");

            }
            new TipClickQuery(_this.morenation, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());

        } else {
            _this.morenation.parents("li").hide();
        }

    }

    var Fule = new Object();
    Fule.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.morefule = $("#morefule");
        this.pop = $("#multipmorefule");
        this.btngroup = $("#btnfulegroup");
        this.btnsurce = $("#btnfulesurce");
        this.btncancel = $("#btnfulecancle");
        _this.loadCarFule(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsurce.on("click", function () {

            var fuleid = [];
            _this.pop.find(":checked").each(function () {
                fuleid.push(Tool.GetNumber($(this).attr("fuleid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.Fuel = fuleid;

            var url = solrfilterinfo.BuilderUrl(101178);
            window.location = url;
        });
        this.btncancel.on("click", function () {
            _this.pop.find(":checked").prop("checked", false).prop("disabled", false);
            _this.pop.addClass("fn-hide").hide();

            _this.morefule.parent().removeClass("active");
        });
    }
    Fule.Hide = function () {
        $("#morefule").parents("li").hide();
    }
    Fule.loadCarFule = function (data) {
        var _this = this;
        //[0]名称[1]url[2]id [3]是否选中
        data = eval(data); var num = 0;
        var checked = false;
        var sb = new StringBuffer();
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                num++;
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
                sb.AppendFormat("<label><input type='checkbox' fuleid='{1}' {2}>{0}</label>", data[i][0], data[i][2], Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "");
            }
        }
        if (num > 1) {
            if (checked) {
                _this.morefule.css("border", "solid 1px #4680d1");
                _this.morefule.children("span").css("color", "#4680d1");
                _this.morefule.children("i").css("color", "#4680d1");

            }
            new TipClickQuery(_this.morefule, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());

        } else {
            _this.morefule.parents("li").hide();
        }

    }
    var Seat = new Object();
    Seat.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.more = $("#moresteat");
        this.pop = $("#multipmoresteat");
        this.btngroup = $("#btnsteatgroup");
        this.btnsure = $("#btnsteatsurce");
        this.btncancle = $("#btnsteatcancle");

        _this.loadCarSeat(data);
        var data = eval(data);
        this.datalen = data.length;

        this.btnsure.on("click", function () {

            var seatid = [];
            _this.pop.find(":checked").each(function () {
                seatid.push(Tool.GetNumber($(this).attr("seatid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.Seat = seatid;

            var url = solrfilterinfo.BuilderUrl(101179);
            window.location = url;
        });
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false).prop("disabled", false);
            _this.pop.addClass("fn-hide").hide();

            _this.more.parent().removeClass("active");
        });
    }
    Seat.Hide = function () {
        $("#moresteat").parents("li").hide();
    }
    Seat.loadCarSeat = function (data) {
        var _this = this;
        //[0]名称 [1]url [2]id [3]是否被选中
        data = eval(data); var num = 0;
        var checked = false;
        var sb = new StringBuffer();
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][1])) {
                num++;
                if (Tool.GetNumber(data[i][3]) == 1) {
                    checked = true;
                }
                sb.AppendFormat("<label><input type='checkbox' seatid='{1}' {2}>{0}</label>", data[i][0], data[i][2], Tool.GetNumber(data[i][3]) == 1 ? "checked='checked'" : "");
            }
        }
        if (num > 1) {
            if (checked) {
                _this.more.css("border", "solid 1px #4680d1");
                _this.more.children("span").css("color", "#4680d1");
                _this.more.children("i").css("color", "#4680d1");
            }
            new TipClickQuery(_this.more, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });
            _this.btngroup.append(sb.ToString());

        } else {
            _this.more.parents("li").hide();
        }

    }
    var Option = new Object();
    Option.init = function (data) {
        var _this = this;
        this.isload = false;
        this.filterval = $("#filterInfoJs").val();
        this.pop = $("#multipmoreoption");
        this.more = $("#moreopton");
        this.btngroup = $("#btnoptiongroup");
        this.btnsure = $("#btnoptionsurce");
        this.btncancle = $("#btnoptioncancle");
        _this.loadCarOption(data);
        var data = eval(data);
        this.datalen = data.length;
        this.btnsure.on("click", function () {

            var optionid = [];
            _this.pop.find(":checked").each(function () {
                optionid.push(Tool.GetNumber($(this).attr("optionid")));
            });
            var solrfilterinfo = SolrFilterInfo.Convert(_this.filterval);
            solrfilterinfo.Option = optionid;

            var url = solrfilterinfo.BuilderUrl(101180);
            window.location = url;
        })
        this.btncancle.on("click", function () {
            _this.pop.find(":checked").prop("checked", false).prop("disabled", false);
            _this.pop.addClass("fn-hide").hide();

        });
    }
    Option.Hide = function () {
        $("#moreopton").parents("li").hide();
    }
    Option.loadCarOption = function (data) {
        var _this = this;
        //[0]名称 [1]短名称 [2]url [3]id [4]是否被选中
        data = eval(data);
        var sb = new StringBuffer();
        var num = 0;
        var checked = false;
        for (var i = 0; i < data.length; i++) {
            if (!string.IsNullOrEmpty(data[i][2])) {
                var opname = data[i][0];
                if (opname.length > 8) {
                    opname = opname.substring(0, 6);
                }
                if (Tool.GetNumber(data[i][4]) == 1) {
                    checked = true;
                }
                num++;
                sb.AppendFormat("<label title=\"{3}\"><input type='checkbox' optionid='{1}' {2}>{0}</label>", opname, data[i][3], Tool.GetNumber(data[i][4]) == 1 ? "checked='checked'" : "", data[i][0]);
            }
        }
        if (num > 1) {
            if (checked) {
                _this.more.css("border", "solid 1px #4680d1");
                _this.more.children("span").css("color", "#4680d1");
                _this.more.children("i").css("color", "#4680d1");
            }
            new TipClickQuery(_this.more, _this.pop, "", "", function (more, pop) {
                more.parent().addClass("active"); pop.removeClass("fn-hide"); more.parents("div.condition-list").css("z-index", "202");
            }, function (more, pop) { pop.addClass("fn-hide"); more.parent().removeClass("active"); more.parents("div.condition-list").css("z-index", "0px"); });

            _this.btngroup.append(sb.ToString());

        } else {
            _this.more.parents("li").hide();
        }

    }
}
Filter();

var ChangeCity = new Object();
ChangeCity.init = function () {
    var _this = this;
    this.provincecity = $(".city-municipality");//直辖市
    this.hotprovincecity = $(".city-hotcity");
    this.city = $("#div_Area");
    this.filterval = $("#filterInfoJs").val();
    this.provincecity.find("a").each(function () {
        var url = _this.Change($(this).attr("href"));
        $(this).attr("href", url);
    });
    this.hotprovincecity.find("a").each(function () {
        var url = _this.Change($(this).attr("href"));
        $(this).attr("href", url);
    });
    this.city.find("a").each(function () {
        var url = _this.Change($(this).attr("href"));
        $(this).attr("href", url);
    });
}
ChangeCity.Change = function (url) {
    url = url.replace("http:", "").replace("//www.che168.com", "");
    var array = url.split("/");
    var areapinyin = array[1];
    var pv = array[2];
    pv = this.GetPv(pv);
    var solrfilterinfo = SolrFilterInfo.Convert(this.filterval);
    solrfilterinfo.AreaPinyin = areapinyin;
    return solrfilterinfo.BuilderUrl(Tool.GetNumber(pv));
}
ChangeCity.GetPv = function (url) {
    var pv = [];
    for (var i = 0; i < url.length; i++) {
        if (Tool.IsNumber(url[i])) {
            pv.push(url[i]);
        }
    }
    return pv.join("");
}
ChangeCity.init();

/*
$(".newcarprice").each(function (i, v) {
    var specid = Tool.GetNumber($(this).attr("specid"));
    var cityid = Tool.GetNumber($(this).attr("cid"));
    if (specid > 0 && cityid > 0) {
        C.loadJS('http://www.interface.che168.com/quoted/dealerminpricebyspec.ashx?specid=' + specid + '&cityid=' + carcid + '&_callback=load4SPriceCallBack' + i, null);
    }
});
load4SPriceCallBack0 = function () {
    if (calldata.returncode == 0 && calldata.result.price > 0) {
        var newcarprice = Tool.GetNumber(calldata.result.price) + Tool.GetNumber(calldata.result.tax);
        var carprice = $(".newcarprice").eq(0);
        var price = Tool.GetNumber(divcarpriceattr("price")) * 100
        var more = Tool.GetNumber((newcarprice * 100 - carprice) / 100);
        if (more > 0) {
            if (calldata.result.tax) {
                carprice.html("比新车省：" + more.toFixed(2) + "万");
            }
            else {
                $("#newcarprice").show().html("比新车省：" + more.toFixed(2) + "万");
            }
        }
    }

}
load4SPriceCallBack1 = function () {
    if (calldata.returncode == 0 && calldata.result.price > 0) {
        var newcarprice = Tool.GetNumber(calldata.result.price) + Tool.GetNumber(calldata.result.tax);
        var carprice = $(".newcarprice").eq(1);
        var price = Tool.GetNumber(divcarpriceattr("price")) * 100
        var more = Tool.GetNumber((newcarprice * 100 - carprice) / 100);
        if (more > 0) {
            if (calldata.result.tax) {
                carprice.html("比新车省：" + more.toFixed(2) + "万");
            }
            else {
                $("#newcarprice").show().html("比新车省：" + more.toFixed(2) + "万");
            }
        }
    }
}
load4SPriceCallBack2 = function () {
    if (calldata.returncode == 0 && calldata.result.price > 0) {
        var newcarprice = Tool.GetNumber(calldata.result.price) + Tool.GetNumber(calldata.result.tax);
        var carprice = $(".newcarprice").eq(2);
        var price = Tool.GetNumber(divcarpriceattr("price")) * 100
        var more = Tool.GetNumber((newcarprice * 100 - carprice) / 100);
        if (more > 0) {
            if (calldata.result.tax) {
                carprice.html("比新车省：" + more.toFixed(2) + "万");
            }
            else {
                $("#newcarprice").show().html("比新车省：" + more.toFixed(2) + "万");
            }
        }
    }
}
load4SPriceCallBack3 = function () {
    if (calldata.returncode == 0 && calldata.result.price > 0) {
        var newcarprice = Tool.GetNumber(calldata.result.price) + Tool.GetNumber(calldata.result.tax);
        var carprice = $(".newcarprice").eq(3);
        var price = Tool.GetNumber(divcarpriceattr("price")) * 100
        var more = Tool.GetNumber((newcarprice * 100 - carprice) / 100);
        if (more > 0) {
            if (calldata.result.tax) {
                carprice.html("比新车省：" + more.toFixed(2) + "万");
            }
            else {
                $("#newcarprice").show().html("比新车省：" + more.toFixed(2) + "万");
            }
        }
    }
}
*/
/*异地车源*/
RemoteVechicleSource();
/*喜欢车源*/
GuessControl();
/*热门车系 车型*/
HotSeriesAndSpec();

$(document).ready(function () {
    $('#filtershow_mcarsend').html($('#mcarsmswords').val());
    $("#mobile").bind("focus", function () {
        if ($(this).val() == "手机号") {
            $(this).val("");
        }
    }).bind("blur", function () {
        if ($(this).val() != "手机号" && !string.IsNullOrEmpty($(this).val())) {
            $("#mCarSMSSend").removeClass("disabled");
        }
    });
    $("#mobilecode").bind("focus", function () {
        if ($(this).val() == "验证码") {
            $(this).val("");
        }
    })
    $('#mobile').bind('keyup', function () { if ($(this).val().length >= 11) validateMCarMsgMobile(); }).bind('change', function () { validateMCarMsgMobile(); }).bind('blur', function () { if ($(this).val().length != 0) validateMCarMsgMobile(); });
    $('#mobilecode').bind('keyup', function () { if ($(this).val().length >= 6) validateMCarMsgMobileCode(); }).bind('change', function () { validateMCarMsgMobileCode(); }).bind('blur', function () { if ($(this).val().length != 0) validateMCarMsgMobileCode(); });
    if ($.browser.msie && $.browser.version == 6)
        $('#mCarSMSSend').attr('href', '##');
    $("#bookclose").bind("click", function () {
        Pop.Dialog.Hide('custommcarsend');
    });
    $("#bookclosetip").bind("click", function () {
        Pop.Dialog.Hide('msgnotity');
    });
});

var sendMCarMsgEnabled = true;
function sendMCarMsg() {
    if (!sendMCarMsgEnabled) {
        return false;
    }
    if (!validateMCarMsgMobile()) {
        return false;
    }
    sendMCarMsgEnabled = false;
    $.ajax({
        url: ['/handler/usedcarlistv5.ashx?action=sendmsgcode&mobile=', $('#mobile').val(), '&mobilecode=', $('#mobilecode').val()].join(''),
        dataType: 'json',
        success: function (data) {
            if (data) {
                if (data.returncode == 0) {
                    setTimeout(function () { stopping = false; mCarMsgCountdown(59); }, 1000);
                    $('#custommcarsend .mt10').find('span').hide().prev().show().html('系统已向您发送验证码，<br>如未收到，请联系客服010-59874131获取。');
                    return;
                }
                else if (data.returncode == 102) {
                    $('#v_mobile').show().html('<i class="ic-wrong"></i>请输入正确的手机号');
                    $('#t_mobile').hide();
                    return;
                }
                else if (data.returncode == 2040001) {
                    $('#custommcarsend .mt10').find('span').hide().prev().show().html(data.message.replace('次）。', '次）。<br/>'));
                    $('#mCarSMSSend').text('已发送').addClass('disabled ');
                    sendMCarMsgEnabled = false;
                    return;
                }
            }
            alert(data ? data.message : '验证码发送错误');
            $('#mCarSMSSend').text('发送验证码');
            sendMCarMsgEnabled = true;
        },
        error: function () {
            $('#mCarSMSSend').text('发送验证码');
            sendMCarMsgEnabled = true;
        }
    });
}

function addCustomMCarSend() {
    if (!validateMCarMsgMobile()) {
        return false;
    }
    if (!validateMCarMsgMobileCode()) {
        return false;
    }

    $.ajax({
        url: ['/handler/usedcarlistv5.ashx?action=addcustommcarsend&mobile=', $('#mobile').val(), '&mobilecode=', $('#mobilecode').val(), '&', $('#mcarsmsurl').val()].join(''),
        dataType: 'json',
        success: function (data) {
            if (data) {
                if (data.returncode == 0) {
                    Pop.Dialog.Hide('custommcarsend');
                    Pop.Dialog('msgnotity');
                    return;
                }
                else if (data.returncode == 102) {
                    $('#error').show().html('请输入正确的手机号');

                    return;
                }
                else if (data.returncode == 2040002) {
                    $('#error').show().html('请输入正确的验证码');
                    return;
                }
                else if (data.returncode == 2040003) {
                    $('#error').show().html('保存失败');
                }
            }
            alert(data ? data.message : '订阅失败，请重试。');
        }
    });
}

var stopping = false;
var countdowning = false;
function mCarMsgCountdown(second) {
    countdowning = true;
    $('#mCarSMSSend').text([second, 's'].join('')).addClass("disabled");
    if (stopping || second <= 1) {
        $('#mCarSMSSend').text('重新发送');
        sendMCarMsgEnabled = true;
        countdowning = false;
        $("#mCarSMSSend").removeClass("disabled");
    } else {
        setTimeout('mCarMsgCountdown(' + (second - 1) + ')', 1000);
    }
}

function validateMCarMsgMobile() {
    if (/^1\d{10}$/.test($('#mobile').val())) {

        if (countdowning == false) {
            $('#mCarSMSSend').text('发送验证码').removeClass('disabled');
            sendMCarMsgEnabled = true;
        }
        $('#error').hide();
        return true;
    } else {
        $('#error').show().html('<span class="icon-error"><i class="iconfont iconfont-close">&#xe90c;</i></span>请输入正确的手机号');
        return false;
    }
}

function validateMCarMsgMobileCode() {
    if (/^\d{6}$/.test($('#mobilecode').val())) {

        return true;
    } else {
        $('#error').show().html('<span class="icon-error"><i class="iconfont iconfont-close">&#xe90c;</i></span>请输入正确的验证码');
        return false;
    }
}

function showCustomMCarMsg() {
    $('#mobile').val('手机号');
    $('#mobilecode').val('验证码');
    $('#error').hide();

    Pop.Dialog('custommcarsend');
    $('#mCarSMSSend').text('发送验证码');
    sendMCarMsgEnabled = true;
    $('#custommcarsend .mt10').find('span').show().prev().hide();
    stopping = true;
}



// 组合车系
var seriesCombine = function () {
    var divSeriesCombineId = "divSeriesCombine";
    var aSeriesCombineId = "#aSeriesCombine";
    var aCancelSeriesCombineId = "#aCancelSeriesCombine";
    var aCloseSeriesCombineId = "#aCloseSeriesCombineId";
    var sh_divSCBrandSeries1Id = "#sh_divSCBrandSeries1";
    var sh_divSCBrandSeries2Id = "#sh_divSCBrandSeries2";
    var sh_divSCBrandSeries3Id = "#sh_divSCBrandSeries3";
    var sh_divSCBrandSeries1_popId = "sh_divSCBrandSeries1_pop";
    var sh_divSCBrandSeries2_popId = "sh_divSCBrandSeries2_pop";
    var sh_divSCBrandSeries3_popId = "sh_divSCBrandSeries3_pop";
    var liAddSeriesCombineId = "#liAddSeriesCombine";
    var liSCBrand3 = "#liSCBrand3";
    var divSeriesCombineErrorId = "#divSeriesCombineError";
    var aShowSeriesCombineId = "#aShowSeriesCombine";
    var aCloseSeriesCombine3 = "#aCloseSeriesCombine3";

    var scbs1 = new scBrandSeries(1);
    var scbs2 = new scBrandSeries(2);
    var scbs3 = new scBrandSeries(3);

    // 重置选择项
    var resetSelect = function () {
        for (var i = 1; i <= 3; i++) {
            $("#divSCBrandSeries" + i).html("请选择车系");
            $("#sh_divSCBrandSeries" + i + "_pop").hide();
        }
        scbs1.Series.SeriesPy = scbs2.Series.SeriesPy = scbs3.Series.SeriesPy = '';
        $(liAddSeriesCombineId).show();
        $(liSCBrand3).hide();
        $(divSeriesCombineErrorId).hide();

    };

    // 初始化
    this.init = function () {
        $(aSeriesCombineId).click(function () {
            Pop.Dialog(divSeriesCombineId);
        });

        $(aCancelSeriesCombineId).click(function () {
            $("#" + divSeriesCombineId).hide();
            Pop.Dialog.Hide(divSeriesCombineId);
            resetSelect();
        });

        $(aCloseSeriesCombineId).click(function () {
            $("#" + divSeriesCombineId).hide();
            Pop.Dialog.Hide(divSeriesCombineId);
            resetSelect();
            $("#divSeriesCombine").find(".active").removeClass("active");
        });

        $(sh_divSCBrandSeries1Id).click(function (event) {
            $("#" + sh_divSCBrandSeries2_popId).hide();
            $("#" + sh_divSCBrandSeries3_popId).hide();
            $("#" + sh_divSCBrandSeries1_popId).show();
            $("#divSeriesCombine").find(".active").removeClass("active");
            $(this).addClass("active");
        });

        $(sh_divSCBrandSeries2Id).click(function () {
            $("#" + sh_divSCBrandSeries1_popId).hide();
            $("#" + sh_divSCBrandSeries3_popId).hide();
            $("#" + sh_divSCBrandSeries2_popId).show();
            $("#divSeriesCombine").find(".active").removeClass("active");
            $(this).addClass("active");
        });

        $(sh_divSCBrandSeries3Id).click(function () {
            $("#" + sh_divSCBrandSeries1_popId).hide();
            $("#" + sh_divSCBrandSeries2_popId).hide();
            $("#" + sh_divSCBrandSeries3_popId).show();
            $(".active").removeClass("active")
            $(this).addClass("active");
        });

        $(liAddSeriesCombineId).click(function () {
            $(liAddSeriesCombineId).hide();
            $(liSCBrand3).show();
        });

        $(aCloseSeriesCombine3).click(function () {
            $(liAddSeriesCombineId).show();
            $(liSCBrand3).hide();
            $("#divSCBrandSeries3").html("请选择车系");
            scbs3.Series.SeriesPy = '';
        });

        // 点击查看组合车系
        $(aShowSeriesCombineId).click(function () {
            var seriesPy = [];
            var cnt = 0;
            if (scbs1.Series.SeriesPy) {
                cnt++;
                seriesPy.push(scbs1.Series.SeriesPy);
            }

            if (scbs2.Series.SeriesPy) {
                cnt++;
                seriesPy.push(scbs2.Series.SeriesPy);
            }

            if (scbs3.Series.SeriesPy) {
                cnt++;
                seriesPy.push(scbs3.Series.SeriesPy);
            }

            // 验证车系选择
            if (cnt < 2) {
                $(divSeriesCombineErrorId).find("span").html("请至少选择两种车系");
                $(divSeriesCombineErrorId).show();

                return;
            }
            else if (scbs1.Series.SeriesPy == scbs2.Series.SeriesPy || scbs1.Series.SeriesPy == scbs3.Series.SeriesPy || scbs2.Series.SeriesPy == scbs3.Series.SeriesPy) {
                $(divSeriesCombineErrorId).find("span").html("请选择不同的车系");
                $(divSeriesCombineErrorId).show();

                return;
            }

            // 使用MoreSelect.js中的方法构造url
            var solrfilter = SolrFilterInfo.Convert($("#filterInfoJs").val());

            solrfilter.BrandPinyin = "nonebrand";
            solrfilter.SeriesPinyin = seriesPy.join('-');
            var url = solrfilter.BuilderUrl(101758);
            $(aCloseSeriesCombineId).click();
            location.href = url;
        });
    };
};

// 组合车系-选品牌、车系
var scBrandSeries = function (index) {
    var _this = this;
    var scBrandSeriesTitle = $('#divSCBrandSeries' + index);
    var scBrandSeries = $('#sh_divSCBrandSeries' + index);
    var scBrandSeriesPop = $('#sh_divSCBrandSeries' + index + '_pop');
    var scBrandListBlock = $("#divSCBrandListBlock" + index);
    var scBrandList = $('#divSCBrandList' + index);
    var scSeriesListBlock = $('#divSCSeriesListBlock' + index);
    var scSeriesList = $('#divSCSeriesList' + index);
    var scBrandListClose = $('#aSCBrandListClose' + index);
    var scLetters = $('#divSCLetters' + index);

    /* 设置A-Z标签定位 */
    var setLetterAZ = function (event) {
        var curletters = $.trim(this.innerHTML);
        var carletters = $(scBrandList).find('dl');
        var totalHigh = 0;
        for (var i = 0; i < carletters.length; i++) {
            if (carletters.eq(i).find('dt').html() == curletters) {
                break;
            }
            else {
                totalHigh += (carletters.eq(i).find('dd>a').length + 1) * 26.3;
            }
        }
        $(scBrandList).scrollTop(totalHigh);
    };

    /*页面初始加载*/
    var init = function () {
        scBrandSeries.bind('click', function (event) {
            scBrandListBlock.show();
            scBrandList.show();

            scSeriesListBlock.show();
            scSeriesList.show();

            _this.Brand.init();
            scBrandSeriesPop.show();
        });

        scLetters.find('a').bind('click', setLetterAZ);
        scBrandListClose.bind('click', function (event) {
            scBrandSeries.removeClass("active");
            scBrandSeriesPop.hide();
            event.stopPropagation();
        });
        scBrandSeriesPop.bind("click", function (event) {
            event.stopPropagation();
        });
    };
    init();

    //品牌

    this.Brand = {};
    this.Brand.selectBrandId = 0;//选中的品牌    
    this.Brand.oldBrandId = 0;
    this.Brand.selectBrand = null;
    this.Brand.BrandName = "";
    this.brandDealyTimeId = 0;
    this.Brand.init = function () {

        if ((this.selectBrandId != this.oldBrandId) || this.oldBrandId == 0) {
            $.getJSON('/handler/usedcarlistv5.ashx?action=brandlist&' + $('#filterInfo').val() + (ie6 ? "&ie6" : ""),
                function (data) {
                    if (!data || data.length <= 0) {
                        return;
                    }

                    var brandList = data;
                    var gblist = '', blist = '', bletter = brandList[0].letters;
                    for (var i = 0; i < brandList.length; i++) {
                        var letter = brandList[i].letters;
                        if (bletter == letter) {
                            blist += '<a href="javascript:void(0);" brandid="' + brandList[i].id + '" filter=\"' + brandList[i].seriesurl + '\" brandpy=\"' + brandList[i].pinyin + '\">' + brandList[i].name + '</a>';
                        } else {
                            gblist += ' <dl class="town-con-dl"><dt>' + bletter + '</dt><dd class="town-btn">' + blist + '</dd></dl>';
                            bletter = brandList[i].letters;
                            blist = '<a href="javascript:void(0);" brandid="' + brandList[i].id + '" filter=\"' + brandList[i].seriesurl + '\" brandpy=\"' + brandList[i].pinyin + '\">' + brandList[i].name + '</a>';
                        }

                        if (brandList.length == i + 1) {
                            gblist += ' <dl class="town-con-dl"><dt>' + bletter + '</dt><dd class="town-btn">' + blist + '</dd></dl>';
                        }
                    }
                    scBrandList.html(gblist);
                    if (_this.Brand.selectBrandId != 0) {
                        scBrandList.find("a").each(function () {
                            if ($(this).attr("brandid") == _this.Brand.selectBrandId) {
                                $(this).addClass("selected");
                            }
                        });
                    }
                    scBrandList.find('dl>dd>a').bind('mouseover', function () {
                        scBrandList.find('.selected').removeClass("selected");
                        scSeriesList.find(".selected").removeClass("selected");
                        var hthis = this;
                        if (_this.brandDealyTimeId != 0)
                            clearTimeout(_this.brandDealyTimeId);
                        _this.brandDealyTimeId = setTimeout(function () {
                            $(hthis).addClass("selected");
                            _this.Brand.BrandName = $(hthis).html();
                            _this.Brand.oldBrandId = _this.Brand.selectBrandId;
                            _this.Brand.selectBrandId = $(hthis).attr("brandid");
                            _this.Brand.selectBrand = $(hthis);
                            _this.Series.init(hthis);
                        }, 40);
                    });
                    scBrandList.bind("dl>dd>a").bind("mouseout", function () {
                        scBrandList.find('.selected').removeClass("selected");
                        clearTimeout(_this.brandDealyTimeId);
                        _this.brandDealyTimeId = 0;
                    });
                });
        }
    };

    //车系
    this.Series = {};
    this.Series.selectSeriesid = 0;//选中的车系
    this.Series.selectSeries = null;
    this.Series.oldSeriesid = 0;//之前选中的车系
    this.Series.SeriesName = "";
    this.Series.SeriesPy = "";
    this.SeriesDelyTimeId = 0;
    this.Series.init = function (obj) {
        if (_this.Brand.selectBrandId != _this.Brand.oldBrandId) {
            $.getJSON('/handler/usedcarlistv5.ashx?action=serieslist&' + $(obj).attr('filter') + (ie6 ? "&ie6" : ""),
                function (data) {
                    scSeriesList.empty();
                    if (!data || data.length <= 0) {
                        return;
                    }
                    var brandPy = $(obj).attr("brandpy");
                    var brandName = $(obj).html();
                    var gblist = '', blist = '';
                    var fName = data[1].fact == "" ? "其它" : data[1].fact;

                    for (var i = 1; i < data.length; i++) {
                        var factoryName = data[i].fact == "" ? "其它" : data[i].fact;
                        if (fName == factoryName) {
                            blist += '<a href="javascript:void(0);" seriesid="' + data[i].id + '" seriespy=\"' + data[i].pinyin + '\">' + data[i].name + '</a>';
                        }

                        if (fName != factoryName) {
                            gblist += ' <dl class="town-con-dl"><dt>' + fName + '</dt><dd class="town-btn">' + blist + '</dd></dl>';
                            fName = factoryName;
                            blist = '<a href="javascript:void(0);" seriesid="' + data[i].id + '" seriespy=\"' + data[i].pinyin + '\">' + data[i].name + '</a>';
                        }

                        if (data.length == i + 1) {
                            gblist += ' <dl class="town-con-dl"><dt>' + fName + '</dt><dd class="town-btn">' + blist + '</dd></dl>';
                        }
                    }
                    scSeriesList.html(gblist);
                    scSeriesList.find('dl>dd>a').bind('click', function () {
                        _this.Series.SeriesPy = brandPy + $(this).attr("seriespy");
                        _this.Series.selectSeriesid = $(this).attr("seriesid");
                        $(scBrandSeriesTitle).html(brandName + ' ' + $(this).html());
                        scBrandSeries.removeClass("active");
                        scBrandSeriesPop.hide();
                    });
                }
            );
        }
    };
};


/**************************************************************************上牌日期*************************************************************************/
/*日期*/
var SelectDay = {};
SelectDay.yearList = $("#divyearList");//年列表
SelectDay.monthList = $("#divmonthList");//月列表
SelectDay.Contain = $("#div_registDatePop");//容器
SelectDay.ShowdayTitle = $("#span_registerDate");//显示日期
SelectDay.divRegistDateJia = $("#div_registDateJia");//显示日期 
SelectDay.selectYear = 0;
SelectDay.selectMonth = 0;
SelectDay.selectItemYear = null;
SelectDay.selectItemMonth = null;
SelectDay.requestDay = true;
SelectDay.isinit = true;
SelectDay.parent = null;


SelectDay.loadYear = function (year, month, minYear) {
    var yearHTML = '', monthHTML = '';
    for (var i = year; i >= minYear ; i--) {
        yearHTML += "<a href=\"javascript:void(0)\" year=" + i + ">" + i + "年</a>\r\n";
    }

    this.yearList.html(yearHTML);
    /*移入移出效果*/
    this.yearList.find("a").bind("mouseover", function () {
        var p = $(this);
        var y = p.attr('year');
        monthHTML = ''
        $('#divyearList').find('a').attr('class', '');
        p.attr('class', 'selected');
        var num = 12;
        if (y == year) { //当前年份，只显示到当前月份
            num = month;
        }
        for (var i = 1; i <= num; i++) {
            monthHTML += "<a onclick=\"SelectDay.SetMonth(" + y + "," + i + ")\" href=\"javascript:void(0)\" month=" + i + ">" + i + "月</a>\r\n";
        }
        SelectDay.monthList.html(monthHTML);
    });
}
//月份点击事件
SelectDay.SetMonth = function (year, month) {
    SelectDay.selectYear = year;
    SelectDay.selectMonth = month;
    if (SelectDay.selectYear == 0 && SelectDay.selectMonth == 0) {
        SelectDay.ShowdayTitle.text(" 首次上牌时间");
    }
    else {
        SelectDay.ShowdayTitle.text(year + "年" + (month >= 10 ? month : "0" + month) + "月");
        SelectDay.ShowdayTitle.attr('year', year);
        SelectDay.ShowdayTitle.attr('month', month);
        $("#div_registDateJia").removeClass("active");
        SelectDay.yearList.find('a[year="' + year + '"]').attr('class', 'selected');
        SelectDay.monthList.find('a[month="' + month + '"]').attr('class', 'selected');
        $("#divDayErrorJia").hide();
    }
    SelectDay.hide();


};
//初始化
SelectDay.init = function (year, month, minYear) {
    this.isinit = false;
    this.Contain.hide();
    this.loadYear(year, month, minYear);
    $("#div_registDate").click(function () {
        $("#sh_divSCBrandSeries50_pop").hide();
        SelectDay.Contain.show();
        $(this).parent().addClass("active");
    });
    $("#div_registDatePop_close").click(function () {
        SelectDay.hide();
    });
}

//隐藏
SelectDay.hide = function () {
    this.Contain.hide();
    this.divRegistDateJia.removeClass('active');
    if (!this.isinit) {
        //this.vail();
        this.isinit = false;
    }

}
//清除
SelectDay.clear = function () {
    if (this.selectItemYear != null) {
        this.selectItemYear.removeClass("selected");
    }
    this.selectYear = 0;
    this.selectMonth = 0;
    this.selectItemYear = null;
    this.selectItemMonth == null;
    this.monthList.empty();
    this.isinit = true;
    this.fillData();
}


var div_car = $('#div_car'), div_carPop = $('#div_carPop'), div_brandList = $('#div_brandList'),
    div_seriesList = $('#div_seriesList'),
    div_pletters = $('#div_pletters');
var isJiaJiaSellCar = $("#isJiaJiaSellCar").val();

/*选择车型 begin*/
var CarInfo = new Object();
CarInfo.selectBrand = null;//记录鼠标移入的品牌对象
CarInfo.BrandDelyTimeId = 0;//移入品牌加载车系的延时秒数
CarInfo.selectSeries = null;//记录鼠标移入的车系对象
CarInfo.SeriesDelyTimeId = 0;//移入车系加载车型的延时秒数
CarInfo.selectBrandId = 0;
CarInfo.selectSericeId = 0;
CarInfo.selectSpedId = 0;
/*页面操作信息，选中品牌车系车型*/
var isLoadedBrand = false; selectbrandid = 0, selectseriesid = 0, selectspecid = 0, selectYear = 0, selectMonth = 0, sltSeriesName = '', sltSpecName = '';


var SaleCar_Base = function () {

    var uthis = this;

    var div_specList;
    if (isJiaJiaSellCar == 2) {
        div_specList = $('#div_specList');
    }
    /* 设置A-Z标签定位 */
    var setLetterAZ = function () {
        var curletters = $.trim(this.innerHTML);
        var carletters = $('#div_brandList').find('dl');
        var totalHigh = 0;
        for (var i = 0; i < carletters.length; i++) {
            if (carletters.eq(i).find('dt').html() == curletters) {
                break;
            }
            else {
                totalHigh += (carletters.eq(i).find('dd>a').length + 1) * 26.3;
            }
        }
        $('#div_brandList').scrollTop(totalHigh);
    };

    /*加载品牌*/
    var loadBrand = function (slectObj) {
        if (isLoadedBrand) { return; }
        $.ajax({
            url: '/Handler/ScriptCarList_V1.ashx?needData=1',
            dataType: "text",
            success: function (result) {
                eval(result);
                if (typeof fct == 'undefined' || fct['0'].length == 0) { return; }
                var gblist = '', blist = '', bletter = 'A';
                var brandList = fct['0'].split(',');
                var brandName = '';
                for (var i = 0; i < brandList.length; i = i + 2) {
                    brandName = brandList[i] == 35 ? '阿斯顿·马丁' : brandList[i + 1].substring(2);
                    if (slectObj && slectObj.brandId) {
                        if (brandList[i] == slectObj.brandId) {
                            blist += '<a href="javascript:void(0);" id="temp_brandid" clsass="selected" brandid="' + brandList[i] + '">' + brandName + '</a>';
                        }
                        else {
                            blist += '<a href="javascript:void(0);" brandid="' + brandList[i] + '">' + brandName + '</a>';
                        }
                    }
                    else {
                        blist += '<a href="javascript:void(0);" brandid="' + brandList[i] + '">' + brandName + '</a>';
                    }
                    if (brandList.length == i + 2 || brandList[i + 3].substring(0, 1).toUpperCase() != bletter) {
                        gblist += ' <dl class="town-con-dl"><dt>' + brandList[i + 1].substring(0, 1) + '</dt><dd class="town-btn">' + blist + '</dd></dl>';
                        if (brandList.length > i + 2) { bletter = brandList[i + 3].substring(0, 1); }
                        blist = '';
                    }
                }
                div_brandList.html(gblist);
                div_brandList.find("dl>dd>a").bind("mouseover", function () {
                    if (CarInfo.BrandDelyTimeId)
                        clearTimeout(CarInfo.BrandDelyTimeId);
                    var _this = $(this);
                    div_brandList.find('.selected').removeClass("selected");
                    div_seriesList.find('.selected').removeClass("selected");
                    if (isJiaJiaSellCar == 2) { div_specList.find('.selected').removeClass("selected"); }
                    _this.add("selected");
                    CarInfo.BrandDelyTimeId = setTimeout(function () {
                        div_seriesList.empty();
                        if (isJiaJiaSellCar == 2) { div_specList.empty(); }
                        CarInfo.selectBrand = _this
                        loadCarSeries();
                    }, 40);

                }).bind("mouseout", function () {
                    clearTimeout(CarInfo.BrandDelyTimeId);
                    CarInfo.BrandDelyTimeId = 0;
                });
                isLoadedBrand = true;
                if (slectObj && slectObj.brandId) {
                    CarInfo.selectBrand = $("#temp_brandid");
                    loadCarSeries(slectObj);
                }
            }
        });
    };

    /*加载车系*/
    var loadCarSeries = function (slectObj) {
        CarInfo.selectBrand.addClass("selected");
        selectspecid = 0;
        selectseriesid = 0;
        selectbrandid = CarInfo.selectBrand.attr("brandid");
        CarInfo.selectBrandId = selectbrandid;

        div_car.attr('bid', selectbrandid);
        var url1 = '/Handler/ScriptCarList_V1.ashx?seriesGroupType=2&needData=2&bid=' + selectbrandid;
        $.ajax({
            url: url1,
            dataType: "text",
            success: function (result) {
                eval(result);
                if (typeof br[selectbrandid] == 'undefined' || br[selectbrandid].length == 0) return;
                var slArray = br[selectbrandid].split(',');
                var gblist = '', blist = '', factoryname = '', curhref = '', seriessplit = '';
                factoryname = slArray[1].split(' ')[0];
                var factoryName = '';
                for (var i = 0; i < slArray.length; i += 2) {
                    seriessplit = slArray[i + 1].split(' ');
                    factoryName = seriessplit[0];
                    seriessplit.shift();
                    if (slectObj && slectObj.seriesId) {
                        if (slectObj.seriesId == slArray[i]) {
                            blist += '<a href="javascript:void(0);" id="temp_seriesid" title="' + seriessplit.join(' ') + '" seriesid="' + slArray[i] + '" class="selected">' + seriessplit.join(' ') + '</a>';
                        }
                        else {
                            blist += '<a href="javascript:void(0);" title="' + seriessplit.join(' ') + '"  seriesid="' + slArray[i] + '">' + seriessplit.join(' ') + '</a>';
                        }
                    }
                    else {
                        blist += '<a href="javascript:void(0);" title="' + seriessplit.join(' ') + '" seriesid="' + slArray[i] + '">' + seriessplit.join(' ') + '</a>';
                    }
                    if (slArray.length == i + 2 || slArray[i + 3].split(' ')[0] != factoryname) {
                        gblist += ' <dl class="town-con-dl"><dt>' + factoryName + '</dt><dd class="town-btn">' + blist + '</dd></dl>';
                        if (slArray.length > i + 2) { factoryname = slArray[i + 3].split(' ')[0]; }
                        blist = '';
                    }
                }
                div_seriesList.html(gblist);
                $('#div_seriesBlock').show();
                if (isJiaJiaSellCar == 2) {
                    div_seriesList.find("dl>dd>a").bind("mouseover", function () {
                        div_seriesList.find('.selected').removeClass("selected");
                        if (CarInfo.SeriesDelyTimeId)
                            clearTimeout(CarInfo.SeriesDelyTimeId);
                        var _this = $(this);
                        CarInfo.SeriesDelyTimeId = setTimeout(function () {
                            CarInfo.selectSeries = _this;
                            div_specList.empty();
                            loadCarSpec();
                        }, 40);
                    });
                    div_seriesList.find("dl>dd>a").bind("mouseout", function () {
                        clearTimeout(CarInfo.SeriesDelyTimeId);
                        CarInfo.SeriesDelyTimeId = 0;
                    });
                    if (slectObj && slectObj.seriesId) {
                        CarInfo.selectSeries = $("#temp_seriesid");
                        loadCarSpec(slectObj);
                    }
                } else {
                    div_seriesList.find('dl>dd>a').bind('click', function () {
                        div_seriesList.find('.selected').removeClass("selected");
                        selectseriesid = this.getAttribute("seriesid");
                        sltSeriesName = this.getAttribute('title');
                        this.className = 'selected';
                        div_car.attr('sid', selectseriesid);
                        CarInfo.selectSericeId = selectseriesid;
                        div_carPop.hide();
                        $('#divSeriesErrorJia').html('').hide();
                        $("#carSpan").html(sltSeriesName);
                        div_car.parent().removeClass('active');
                    });
                    if (slectObj && slectObj.seriesId) {
                        $("#temp_seriesid").trigger("click")
                    }
                }
            }
        });
    };

    /*加载车型*/
    var loadCarSpec = function (slectObj) {
        selectseriesid = CarInfo.selectSeries.attr("seriesid");
        sltSeriesName = CarInfo.selectSeries.html();
        CarInfo.selectSeries.addClass("selected");
        selectspecid = 0;
        CarInfo.selectSericeId = selectseriesid;

        div_car.attr('sid', selectseriesid);

        var url1 = '/Handler/ScriptCarList_V1.ashx?seriesGroupType=2&needData=3&seriesid=' + selectseriesid;
        $.ajax({
            url: url1,
            dataType: "text",
            success: function (result) {
                var spcArray = eval(result);
                var specHtml = '';
                for (var i = 0; i < spcArray.length; i++) {
                    specHtml += '<dl class="town-con-dl"><dt>' + spcArray[i].year + '</dt><dd class="town-btn">';
                    for (var j = 0; j < spcArray[i].spec.length; j++) {
                        if (slectObj && slectObj.specId) {
                            if (slectObj.specId == spcArray[i].spec[j].id) {
                                specHtml += '<a href="javascript:void(0);" class="selected" id="temp_specid" title="' + spcArray[i].spec[j].name + '" spid="' + spcArray[i].spec[j].id + '">' + spcArray[i].spec[j].name + '</a>';
                            }
                            else {
                                specHtml += '<a href="javascript:void(0);" title="' + spcArray[i].spec[j].name + '" spid="' + spcArray[i].spec[j].id + '">' + spcArray[i].spec[j].name + '</a>';
                            }

                        }
                        else {
                            specHtml += '<a href="javascript:void(0);" title="' + spcArray[i].spec[j].name + '" spid="' + spcArray[i].spec[j].id + '">' + spcArray[i].spec[j].name + '</a>';
                        }
                    }
                    specHtml += '</dd></dl>';
                }
                div_specList.html(specHtml);
                $('#div_specBlock').show();
                div_specList.find('dl>dd>a').bind('click', selectCarSpec);
                if (slectObj && slectObj.specId) {
                    $("#temp_specid").trigger("click")
                }
            }
        });
    };

    /*选择车型事件*/
    var selectCarSpec = function () {
        div_specList.find('.selected').removeClass("selected");
        selectspecid = this.getAttribute("spid");
        sltSpecName = this.getAttribute('title');
        this.className = 'selected';
        div_car.attr('spid', selectspecid);
        CarInfo.selectSpedId = selectspecid;
        div_carPop.hide();
        uthis.vCar();
        div_car.parent().removeClass('active');
        $("#carSpan").html(sltSpecName);

    };
    /*选择车型 end*/

    //验证车型
    this.vCar = function () {
        var bid = parseInt(div_car.attr('bid'), 10);
        var sid = parseInt(div_car.attr('sid'), 10);
        var specid = parseInt(div_car.attr('spid'), 10);
        if (bid > 0 && sid > 0 && specid > 0) {
            $('#divSeriesErrorJia').html('').hide();
            return true;
        }
        $('#divSeriesErrorJia').html('<i class="icon-error">-</i>请填写车型信息').show();
        return false;
    };

    this.init = function () {
        //关闭选择车型浮层事件绑定
        $('#a_closeCar').bind('click', function () { $('#div_carPop').hide(); $('#div_car').parent().removeClass('active'); })

        //加载品牌
        loadBrand();

        /*品牌首字母事件绑定*/
        div_pletters.find('a').bind('click', setLetterAZ);

        //点击车型显示浮层事件
        div_car.bind('click', function () {
            SelectDay.hide();
            $("#div_registDate").parent().removeClass("active");
            div_car.parent().addClass('active'); div_carPop.show();
        });

    };
};


/*获取当前点击事件的Object对象*/
var getEvent = function () {
    if (document.all) {
        return window.event; //如果是ie
    }
    func = getEvent.caller;
    while (func != null) {
        var arg0 = func.arguments[0];
        if (arg0) {
            if ((arg0.constructor == Event || arg0.constructor == MouseEvent)
|| (typeof (arg0) == "object" && arg0.preventDefault && arg0.stopPropagation)) {
                return arg0;
            }
        }
        func = func.caller;
    }
    return null;
};

/*发车错误日志*/
var submitErrorLog = function (errorTypeName) {
    var evt1 = getEvent();
    if (evt1 != null) {
        var obj = evt1.srcElement || evt1.target;
        if (obj && obj.id == 'btnSellCarShow') {
            $.ajax({
                type: 'get',
                dataType: 'jsonp',
                url: 'http://collection.pv.che168.com/collect/page_event.ashx?info=' + errorTypeName + '&eventkey=i_pc_2sc_inputerror_personfc&ref=' + document.referrer + '&cur=' + location.href,
                success: function () { }
            });
        }
    }
};


var SellCar = {};
SellCar.brandid = 0;
SellCar.seriesid = 0;
SellCar.regestyear = 0;
SellCar.regestmonth = 0;
SellCar.specid = 0;
SellCar.ispassvail = true;
SellCar.phone = "";
SellCar.SessionId = "";
SellCar.cid = 0;
SellCar.pid = 0;
SellCar.vailcontain = $("#divSeriesErrorJia");//验证容器
SellCar.vailmessage = $("#divSeriesErrorMsgJia");//错误信息
SellCar.vailcontain2 = $("#divDayErrorJia");//验证容器
SellCar.vailmessage2 = $("#divDayErrorMsgJia");//错误信息
SellCar.vailcontain3 = $("#divPhoneErrorJia");//验证容器
SellCar.vailmessage3 = $("#divPhoneErrorMsgJia");//错误信息



/*获取默认的省份和城市ＩＤ,参数cookie值*/
SellCar.GetArea = function (areaId) {
    if (areaId != null && areaId != '' && parseInt(areaId, 10) > 0) {
        var p = parseInt(areaId / 10000, 10) * 10000;
        if (p == 110000 || p == 500000 || p == 120000 || p == 310000) {//直辖市
            this.pid = p;
            this.cid = p + 100;
        } else {
            if (p == parseInt(areaId, 10)) {//只有省份
                this.pid = p;
            } else {
                this.pid = p;
                this.cid = parseInt(areaId, 10);
            }
        }
    }
};
SellCar.vail = function () {
    var _this = this;
    _this.phone = $("#jiajiaSellCarPhone").val();
    this.ispassvail = false;
    if (this.seriesid == 0) {
        this.vailcontain.show();
        this.vailmessage.attr('text', '请填写车型信息');
        this.ispassvail = false;
        return;
    }
    this.vailmessage.attr('text', "");
    this.vailcontain.hide();

    if (this.regestyear == 0 && this.regestmonth == 0) {
        this.vailcontain2.show();
        this.vailmessage2.attr('text', "请填写首次上牌日期");
        this.ispassvail = false;
        return;
    }
    this.vailmessage2.attr('text', "");
    this.vailcontain2.hide();


    if (this.phone == "" || this.phone == "请填写手机号") {
        this.vailmessage3.html("请填写手机号");
        this.vailcontain3.show();
        this.ispassvail = false;
        return;
    }
    if (this.phone != "") {
        var reg = new RegExp("^1[3,4,5,6,7,8,9]{1}[0-9]{9}$", "ig");
        if (!reg.test(this.phone)) {
            this.vailmessage3.html("请填写11位正确手机号");
            this.vailcontain3.show();
            this.ispassvail = false;
            return;

        }
        if (this.phone.length != 11) {
            this.vailmessage3.html("请填写11位正确手机号");
            this.vailcontain3.show();
            this.ispassvail = false;
            return;
        }
    }
    this.vailmessage3.attr('text', "");
    this.vailcontain3.hide();



    this.ispassvail = true;
}

SellCar.vail2 = function () {
    var _this = this;
    _this.phone = $("#jiajiaSellCarPhone").val();
    _this.ispassvail = false;

    if (selectspecid == 0) {
        _this.vailcontain.show();
        _this.vailmessage.attr('text', "请填写车型信息");
        submitErrorLog('B');
        _this.ispassvail = false;
        return;
    }
    this.vailmessage.attr('text', "");
    this.vailcontain.hide();

    if (SelectDay.selectYear == 0 && SelectDay.selectMonth == 0) {
        _this.vailcontain2.show();
        _this.vailmessage2.attr('text', "请填写首次上牌日期");
        submitErrorLog('P');
        _this.ispassvail = false;
        return;
    }
    _this.vailmessage2.attr('text', "");
    _this.vailcontain2.hide();

    if (_this.phone == "" || _this.phone == "请填写手机号") {
        this.vailmessage3.html("请填写手机号");
        this.vailcontain3.show();
        this.ispassvail = false;
        if (_this.phone == "") { $("#jiajiaSellCarPhone").val('请填写手机号'); }
        submitErrorLog('V');
        return;
    }
    if (_this.phone != "") {
        var reg = new RegExp("^1[3,4,5,6,7,8,9]{1}[0-9]{9}$", "ig");
        if (!reg.test(this.phone)) {
            this.vailmessage3.html("请填写11位正确手机号");
            this.vailcontain3.show();
            this.ispassvail = false;
            submitErrorLog('V');
            return;

        }
        if (this.phone.length != 11) {
            this.vailmessage3.html("请填写11位正确手机号");
            this.vailcontain3.show();
            this.ispassvail = false;
            submitErrorLog('V');
            return;
        }
    }
    _this.vailmessage3.attr('text', "");
    _this.vailcontain3.hide();




    this.ispassvail = true;
}

SellCar.submit = function () {
    var _this = this;

    _this.brandid = CarInfo.selectBrandId
    _this.seriesid = CarInfo.selectSericeId;
    _this.regestyear = SelectDay.selectYear;
    _this.regestmonth = SelectDay.selectMonth;

    this.vail();
    if (this.ispassvail) {
        var _data = new Object();
        _data.brandid = this.brandid;
        _data.seriesid = this.seriesid;
        _data.exceptsaletime = (this.exceptSaleTime == -1 ? 0 : this.exceptSaleTime);
        _data.phone = this.phone;
        _data.year = this.regestyear;
        _data.month = this.regestmonth;
        _data.cid = this.cid;
        _data.pid = this.pid;
        //HideAjax.ajax(HideAjax.receive.submit);
        $.ajax({
            url: "/Handler/Autonomous/AddAutonomousByPCList.ashx",
            data: _data,
            type: "post",
            async: true,
            dataType: "json",
            success: function (data) {
                if (data.statue == 0) { //保存成功
                    alert("信息提交成功，请等待评估师审核");
                    $('#span_registerDate').text('请填写首次上牌日期');
                    $('#carSpan').attr('text', '请填写车型信息');
                    $('#jiajiaSellCarPhone').val('请填写手机号');
                    $('#div_car').attr("bid", "");
                    $('#div_car').attr("sid", "");
                    $('#div_car').attr("spid", "");
                } else {
                    alert(data.mess);
                }
            }
        });
    }
}

SellCar.init = function () {
    var _this = this;


    var CityId = getCookie('userarea', "110000");
    SellCar.GetArea(CityId);
    var saleCar_Base = new SaleCar_Base().init();
    if (isJiaJiaSellCar && isJiaJiaSellCar == 1) {
        SelectDay.init($("#hdyear").val(), $("#hdmonth").val(), parseInt($("#hdyear").val()) - 6);
        $("#JiaJiaSellCarShow").bind('click', function () { Pop.Dialog("divJiaJiaSellCar"); });
        $("#divCloseJiaJiaSellCar").bind('click', function () { Pop.Dialog.Hide('divJiaJiaSellCar'); });

        $("#jiajiaSellCarPhone").bind('focus', function () { if ($(this).val() == '请填写手机号') { $(this).val(''); } });
        $("#jiajiaSellCarPhone").bind('blur', function () { if ($(this).val() == '') { $(this).val('请填写手机号'); } });
        $("#JiaJiaSellCarSubmit").click(function () {
            _this.submit();
        });
    } else {
        SelectDay.init($("#hdyear").val(), $("#hdmonth").val(), 1991);
        var saleCar_Base = new SaleCar_Base().init();
        $("#jiajiaSellCarPhone").bind('focus', function () { if ($(this).val() == '请填写手机号') { $(this).val(''); } });
        $("#jiajiaSellCarPhone").bind('blur', function () { if ($(this).val() == '') { $(this).val('请填写手机号'); } });
        $("#btnSellCarShow").click(function () {
            _this.regestyear = SelectDay.selectYear;
            _this.regestmonth = SelectDay.selectMonth;
            _this.specid = selectspecid;
            _this.vail2();
            if (_this.ispassvail) {
                window.location.href = 'http://i.che168.com/car/add/?cid=' + _this.cid + '&spid=' + _this.specid + '&cdate=' + _this.regestyear + '-' + _this.regestmonth + '&pid=' + _this.pid + '&type=1&mobile=' + _this.phone + "&pvareaid=100835";
            }

        });
    }
}
;

SellCar.init();

var ie = !!window.ActiveXObject;
var ie6 = ie && !window.XMLHttpRequest;
var sc = new seriesCombine();
sc.init();


$("#divSeriesCombine").bind("click", function (event) {
    event.stopPropagation();
})

function loadAuctionDealer() {
    $.ajax({
        url: '/handler/GetBindRecommandDealer.ashx?pagetype=2&userarea=' + areaId + "&v=201703301809",
        dataType: 'JSON',
        success: function (data) {
            var ids = '';
            var array = [];
            array.push("<h4>推荐商家<span class='tag-ad tag-ad-title fn-ml10'>广告</span></h4>");
            if (typeof (data.obj.Auction) != "undefined" && data.obj.Auction.length > 0) {
                array.push(eachRecommandDealer(data.obj.Auction, 1));
            }
            if (typeof (data.obj.avgCar) != "undefined" && data.obj.avgCar.length > 0) {
                array.push(eachRecommandDealer(data.obj.avgCar, 2));
            }
            if (typeof (data.obj.ordinary) != "undefined" && data.obj.ordinary.length > 0) {
                array.push(eachRecommandDealer(data.obj.ordinary, 3));
            }
            $('#recommandCar').html(array.join(''));
        }
    });
}
if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (callback, thisArg) {
        var T, k;
        if (this == null) {
            throw new TypeError(" this is null or not defined");
        }
        var O = Object(this);
        var len = O.length >>> 0; // Hack to convert O.length to a UInt32  
        if ({}.toString.call(callback) != "[object Function]") {
            throw new TypeError(callback + " is not a function");
        }
        if (thisArg) {
            T = thisArg;
        }
        k = 0;
        while (k < len) {
            var kValue;
            if (k in O) {
                kValue = O[k];
                callback.call(T, kValue, k, O);
            }
            k++;
        }
    };
}
function eachRecommandDealer(obj, type) {
    if (obj != null) {
        var array = [];
        var shopType = 0;
        var shopIcon = "";
        var dealername = "";
        obj.forEach(function (o) {
            var showcar = false;
            shopType = o.shopType;
            if (shopType == 2) {
                shopIcon = "<span class='tag-flagship'></span>";
            }
            else {
                if (shopType == 1) {
                    shopIcon = "<span class='tag-gold-medal'  title='二手车之家优质合作商家'>金牌店铺</span>";
                }
                else {
                    shopIcon = "";
                }
            }
            if (type == 1 && typeof (o.car) != "undefined" && o.car.length > 0) {
                showcar = true;
                array.push("<div class=\"dealer-box\">");
            }
            if (o.dealerName.length > 10) {
                dealername = o.dealerName.substr(0, 10);
            }
            else {
                dealername = o.dealerName;
            }
            array.push("<div class=\"dealer-info " + (showcar == true ? "" : " dealer-bottom") + " \"><p class=\"fn-clear\">");
            array.push("<a href=\"/dealer/" + o.DealerId + ".html#pvareaid=101181\" target=\"_blank\" title=\"" + o.dealerName + "\">" + dealername + "</a>");
            array.push(shopIcon + " </p><p>电话：" + o.phone + "<span><em>" + o.SellingCarCount + "</em>辆车</span></p></div>");
            if (showcar) {
                array.push("<ul class=\"dealer-list1 fn-clear\">");
                o.car.forEach(function (u) {
                    array.push("<li><a target=\"_blank\" href=\"" + u.url + "\" title=\"" + u.carName + "\"><img style=\"width:102px; height:77px;\" src=\"" + u.pic + "\"></a><p class=\"car-name\">");
                    array.push("<a target=\"_blank\" href=\"" + u.url + "\">" + u.carName + "</a>");
                    array.push("</p><p class=\"price\"><span>" + u.price + "<em>万元</em></span></p></li>");
                });
                array.push("</ul>");
            }
            if (showcar) {
                array.push("</div>");
            }
        });
        return array.join('');
    }
}

loadAuctionDealer();

/*
Append File:/2sc/pc/TransferCarShowInfoV1.js
*/
var carHtml = "";
var cpcCarShowPars = '';
var currentPos = 1;

$("#viewlist_ul").children("li").each(function (index, obj) {
    var li = $(this);
    if ($.trim(li.html()).length > 0) {
        var cpcPars = li.attr("cpcpars");
        var adid = li.attr("adid");
        var abtest = li.attr("abtest");
        var isRecom = li.attr("isrecom");
        isRecom = isRecom || 0;
        var infoId = li.attr("infoid");
        abtest = abtest || 0;

        if (index == 0) {
            cpcCarShowPars = cpcPars;
        }
        var link = li.find("a:.carinfo").attr("href");

        var isSelled = li.find(".sold-box").length;
        var dealerId = li.attr("dealerid");
        var type = 0;

        if (li.attr('isafteraudit') == 1) {
            type = 10;
        }
        var adid = li.attr("adid");
        adid = adid || 0;
        //	info=商家id|车源id|是否是推荐车|是否已售|车类型|展现位置
        carHtml += dealerId + "^" + infoId + "^" + isRecom + "^" + isSelled + "^" + type + "^" + currentPos + "^" + abtest + "^" + adid + "|";

        var pageIndex = $(".page .current").html();
        //添加参数到车源链接
        if (!pageIndex)
            pageIndex = 1;
        var newLink = link + "#pos=" + currentPos + "#page=" + pageIndex + "#rtype=" + abtest;

        if (cpcPars) {
            newLink = link + "&pos=" + currentPos + "&adid=" + adid + "#page=" + pageIndex + "#cpcpars=" + cpcPars + "#rtype=" + abtest + "#cpcfrom=0";
        }
        //li.find(".info-con").children("a").attr("href", newLink);      
        //li.find(".title").children("a").attr("href", newLink);
        li.find("a:.carinfo").attr("href", newLink);
        //车源点击事件埋点, c_pc_2sc_carlist_carclick_inlist
        li.find("a:.carinfo").bind("click", function () {
            var lii = $(this).parents("li");
            var currentPos = lii.attr("pos");
            var isRecom = lii.attr("isrecom");
            isRecom = isRecom || 0;
            var abtest = lii.attr("abtest");
            abtest = abtest || 0;
            var isSelled = lii.find(".sold-box").length;
            var dealerId = lii.attr("dealerid");
            var infoId = lii.attr("infoid");
            var type = 0;
            if (li.attr('isafteraudit') == 1) {
                type = 10;
            }
            adid = li.attr("adid");
            adid = adid || 0;
            var info = dealerId + "|" + infoId + "|" + isRecom + "|" + isSelled + "|" + type + "|" + currentPos + "|" + abtest + "|" + adid;
            $.ajax({
                type: "GET",
                async: false,
                url: "http://collection.pv.che168.com/collect/page_event.ashx?eventkey=c_pc_2sc_carlist_carclick_inlist&info=" + info + '&ref=' + escape(document.referrer) + '&cur=' + escape(location.href) + '&rm=' + new Date().getTime(),
                dataType: "jsonp"
            });
        });
        currentPos++;
    }
});

$.ajax({
    type: "get",
    url: "http://collection.pv.che168.com/collect/page_event.ashx?eventkey=s_pc_2sc_carlist_carshow&info=" + carHtml.substring(0, carHtml.length - 1) + '&ref=' + escape(document.referrer) + '&cur=' + escape(location.href) + '&rm=' + new Date().getTime(),
    dataType: "jsonp"
});


if (cpcCarShowPars) {
    $.ajax({
        type: "get",
        url: "http://cpc.click.che168.com/show.ashx?info=" + cpcCarShowPars + '&ref=' + escape(document.referrer) + '&cur=' + escape(location.href),
        dataType: "jsonp"
    });
}


/*
Append File:/2sc/pc/dialogNew.js
*/
var fn_onscroll = null;
Pop = {};
Pop.Dialog = function (DivId, Color, isMask) {

    var de = document.documentElement;
    //var seeHight = typeof window.innerHeight == 'undefined' ? de.clientHeight : window.innerHeight;
    var Color = Color || '#000';
    var isMask = isMask || '1';
    var DivObj = document.getElementById(DivId);
    var MaskDivId = 'CmdMaskDiv';
    var MaskIframeId = 'CmdMaskIframe';
    var createEle = function (tagName, doc) {
        var doc = doc || document;
        return doc.createElement(tagName);
    }
    var init = function () {
        var SP = getPostXY();
        DivObj.style.display = 'block';
        DivObj.style.zIndex = '9999';
        DivObj.style.margin = '0';
        DivObj.style.padding = '0';
        DivObj.style.position = 'absolute';
        DivObj.style.top = (SP.scrollTop + de.clientHeight / 2 - DivObj.offsetHeight / 2) + "px";
        DivObj.style.left = (SP.scrollLeft + de.clientWidth / 2 - DivObj.offsetWidth / 2) + "px";
        if (isMask == '1') { create(SP); }
    };
    var getPostXY = function () {
        var de = document.documentElement;
        var winWidth = 0;
        var winHeight = 0;
        // 获取窗口宽度
        if (window.innerWidth)
            winWidth = window.innerWidth;
        else if ((document.body) && (document.body.clientWidth))
            winWidth = document.body.clientWidth;
        // 获取窗口高度
        if (window.innerHeight)
            winHeight = window.innerHeight;
        else if ((document.body) && (document.body.clientHeight))
            winHeight = document.body.clientHeight;
        // 通过深入 Document 内部对 body 进行检测，获取窗口大小
        if (de && de.clientHeight && de.clientWidth) {
            winHeight = de.clientHeight;
            winWidth = de.clientWidth;
        }
        var scrollTop = document.body.scrollTop || (de && de.scrollTop);
        var scrollLeft = document.body.scrollLeft || (de && de.scrollLeft);

        return { 'W': winWidth, 'H': winHeight, 'scrollTop': scrollTop, 'scrollLeft': scrollLeft };
    };
    var create = function (fun) {
        var MaskObj = document.getElementById(MaskDivId);
        if (MaskObj) {
            MaskObj.style.top = (fun.scrollTop + de.clientHeight / 2 - fun.H / 2) + "px";
            MaskObj.style.left = (fun.scrollLeft + de.clientWidth / 2 - fun.W / 2) + "px";
            MaskObj.style.width = fun.W + 'px';
            MaskObj.style.height = fun.H + 'px';
        }
        else {
            var MaskDiv = createEle('div'),
                MaskIframe = createEle('iframe');
            MaskDiv.id = MaskDivId;
            MaskIframe.id = MaskIframeId;
            document.body.appendChild(MaskIframe);
            document.body.appendChild(MaskDiv);
            MaskDiv.style.display = MaskIframe.style.display = 'block';
            MaskDiv.style.background = MaskIframe.style.background = Color;
            MaskDiv.style.position = MaskIframe.style.position = 'absolute';
            MaskDiv.style.width = MaskIframe.style.width = fun.W + 'px';
            MaskDiv.style.height = MaskIframe.style.height = fun.H + 'px';
            MaskDiv.style.top = MaskIframe.style.top = (fun.scrollTop + de.clientHeight / 2 - fun.H / 2) + "px";
            MaskDiv.style.left = MaskIframe.style.left = (fun.scrollLeft + de.clientWidth / 2 - fun.W / 2) + "px";
            MaskDiv.style.filter = MaskIframe.style.filter = 'alpha(opacity=50)';
            MaskDiv.style.opacity = MaskIframe.style.opacity = '.50';
            MaskIframe.style.border = '0';
            MaskIframe.style.filter = 'alpha(opacity=0)';
            MaskIframe.style.opacity = '.0';
            MaskDiv.style.zIndex = '9998';
            MaskIframe.style.zIndex = '9997';
        }
    };
    init();
    fn_onscroll = window.onscroll;
    window.onresize = init;
    window.onscroll = init;

};

Pop.Dialog.Hide = function (DivId) {
    var MaskDivId = 'CmdMaskDiv',
        MaskIframeId = 'CmdMaskIframe';
    if (document.getElementById(MaskDivId)) {
        document.body.removeChild(document.getElementById(MaskDivId));
    }
    if (document.getElementById(MaskIframeId)) {
        document.body.removeChild(document.getElementById(MaskIframeId));
    }
    window.onresize = null;
    window.onscroll = fn_onscroll;
    document.getElementById(DivId).style.display = 'none';
};

/*
Append File:/2sc/pc/ThousandFaces/CarClick.js
*/
$(".transverse-list a,.vertical-list a").click(function () {
    var li = $(this).parent();
    var infoid = $(li).attr("infoid");
    var page = $(li).attr("page");
    var pos = $(li).attr("pos");
    var abtest = $(li).attr("abtest");

    if (abtest && parseInt(abtest) > 0) {
        $.get("/handler/thousandfaces/carclick.ashx?infoid=" + infoid + "&page=" + page + "&pos=" + pos);
    }
});

/*
Append File:/che168/js/PCTrack.js
*/
$("a[name='track'],div[name='track'],span[name='track'],li[name='track']").each(function (i, obj) {
    $(obj).click(function () {
        trackClick($(obj).attr("eventkey"), $(obj).attr("infoid"));
    });
});
// 记录点击
function trackClick(eventKey, info) {

    $.ajax({
        type: "GET",
        url: "http://collection.pv.che168.com/collect/page_event.ashx?eventkey=" + eventKey + '&info=' + info + '&ref=' + document.referrer + '&cur=' + location.href + '&rm=' + new Date().getTime(),
        dataType: "jsonp"
    });
}


/*
Append File:/2sc/pc/UsedCarList/v6/SimilarSeriesCity.js
*/
var lseriesid = 0;
var lcid = 0;
var lpageindex = 1;

var addMoreCar = function () {
    var _this = this;
    var _url = "/handler/CarList/SimilarSeriesCity.ashx";
    this.filter = $("#filterInfoJs");
    this.kw = $("#filterKw");
    this.filterinfo = SolrFilterInfo.Convert(_this.filter.val());
    this.listIcon = [];
    this.isloadCity = false;
    this.isloadSeries = false;
    this.loadCitys = new Array();
    this.loadSeries = new Array();
    this.IsAroundCityCar = $("#IsAroundCityCar").val();
    this.infoList = 0,
    this.changeCid = 0,
    this.loadCar = function () {
        if (this.IsAroundCityCar == "1") {
            var _data = this.filterinfo.ToString();

            $.ajax({
                url: _url,
                data: _data,
                dataType: "json",
                success: function (data) {
                    if ((data.obj != null && data.obj.length != 0) || strCity != '') {
                        if (data != null && data.obj != null && data.obj[0] && data.obj[0].SeriesList != null && data.obj[0].SeriesList.length > 0) {
                            var htmlList = [];
                            htmlList.push("   <a id=\"ser_0\" class=\"current\"  href=\"javascript:void(moreCar.addOtherSesiesCar(0))\">全部</a>");
                            for (var i = 0; i < data.obj[0].SeriesList.length ; i++) {
                                var dl = data.obj[0].SeriesList;
                                htmlList.push(" <a id=\"ser_" + dl[i].seriesid + "\"   href=\"javascript:void(moreCar.addOtherSesiesCar(" + dl[i].seriesid + "))\">" + dl[i].seriesname + "</a>");
                            }
                            _this.infoList = '';
                            $("#tab-1-1").html('').html(htmlList.join(''));
                            htmlList = [];
                            htmlList.push(_this.getCarLiHtml(data.obj[0].carList, 1));
                            _this.loadSeries[0] = htmlList.join('');
                            $("#tab-1-2").html('').html(htmlList.join(''));

                        }
                        else {
                            $("#currentCar").hide();
                            $("#tab-1").hide();
                        }
                        _this.addcurrentCar();
                        _this.getTitle(data, 2);
                    } else {
                        $(".list-source-wrap").hide();
                    }
                }
            });
        }
    },
    this.getTitle = function (data, random) { //目前默认random为2,加载异地车源        
        $("#otherCar").removeClass("current");
        $("#currentCar").removeClass("current");
        $("#tab-2").removeClass("current");
        $("#tab-1").removeClass("current");
        if (strCity != '') {
            if (random == 1) {
                $("#currentCar").addClass("current");
                $("#tab-1").addClass("current");
                _this.isloadSeries = true;
            } else {
                $("#otherCar").addClass("current");
                $("#tab-2").addClass("current");
                _this.isloadCity = true;
                eventkeyADDMore('s_pc_2sc_carlist_otherrecom', $('#otherCarInfo').val());

            }
        } else if (data.obj[0] != null && data.obj[0].SeriesList != null && data.obj[0].SeriesList.length > 0) {
            $("#currentCar").addClass("current");
            $("#tab-1").addClass("current");
            $("#otherCar").hide();
            $("#tab-2").hide();
        }
    },
    /*加载异地城市车源*/
    this.addOtherCityCar = function (cityid) {
        _this.changeCid = cityid;
        var isload = false;
        lcid = cityid;
        for (var key in _this.loadCitys) {
            if (cityid == key) {
                isload = true; break;
            }
        }
        if (isload) {
            var htmlList = _this.loadCitys[cityid];
            $("#tab-2-2").html('').html(htmlList);
            $("[id^=cid_]").removeClass("current");
            $("#cid_" + lcid).addClass("current")
        } else {
            var _data = this.filterinfo.ToString();
            $.ajax({
                url: _url + "?v=20170320&action=otherCar&cityid=" + cityid + "&kw=" + this.kw.val(),
                data: _data,
                dataType: "json",
                success: function (data) {
                    if (data != null && data.obj != null && data.obj[0] != null && data.obj[0].carList.length > 0) {
                        _this.infoList = '';

                        var htmlList = _this.getCarLiHtml(data.obj[0].carList, 2);
                        _this.loadCitys[cityid] = htmlList;
                        $("#tab-2-2").html('').html(htmlList);
                        $("[id^=cid_]").removeClass("current");
                        $("#cid_" + lcid).addClass("current")

                        eventkeyADDMore('s_pc_2sc_carlist_otherrecom', _this.infoList);
                    }
                }
            });
        }

    },
    this.addcurrentCar = function () {
        $("#currentCar").bind("click", function () {
            eventkeyADDMore('s_pc_2sc_carlist_localrecom', _this.infoList);
        });
    },
    /*加载本地相似车系车源*/
 this.addOtherSesiesCar = function (seriesid) {
     var isload = false;
     lseriesid = seriesid;
     for (var key in _this.loadSeries) {
         if (lseriesid == key) {
             isload = true; break;
         }
     }
     if (isload) {
         var htmlList = _this.loadSeries[lseriesid];
         $("#tab-1-2").html('').html(htmlList);
         $("[id^=ser_]").removeClass("current");
         $("#ser_" + lseriesid).addClass("current")
     } else {
         var _data = this.filterinfo.ToString();
         $.ajax({
             url: _url + "?v=20170320&action=currentCar&seriesid=" + seriesid + "&kw=" + this.kw.val(),
             data: _data,
             dataType: "json",
             success: function (data) {
                 if (data != null && data.obj != null && data.obj[0] != null && data.obj[0].carList.length > 0) {
                     _this.infoList = '';
                     var htmlList = _this.getCarLiHtml(data.obj[0].carList, 1);
                     _this.loadSeries[lseriesid] = htmlList;
                     $("#tab-1-2").html('').html(htmlList);
                     $("[id^=ser_]").removeClass("current");
                     $("#ser_" + lseriesid).addClass("current")
                     eventkeyADDMore('s_pc_2sc_carlist_localrecom', _this.infoList);
                 }
             }
         });
     }
 },
    /*拼接车源html*/
    this.getCarLiHtml = function (obj, type) {
        var carlist = [];
        for (var i = 0; i < obj.length; i++) {
            if (i == 48) { break; }
            var carinfo = obj[i];
            carlist.push("<li>");

            var url = carinfo.url;
            //车源id ^ 展现位置 ^ 商家id ^ 是否是推荐车 ^ 是否已售 ^ 车类型 ^ 千人千面推荐类型 ^ 竞价广告id ^ 城市id |
            //车类型：0平台，1家家，2认证
            var carType = 0;
            var info = carinfo.id + "^" + (i + 1) + "^" + carinfo.did + "^" + carinfo.installment + "^" + (carinfo.selled == 5 ? 1 : 0) + "^" + carType + "^" + 0 + "^" + (typeof carinfo.adid == 'undefined' ? 0 : carinfo.adid) + "^" + _this.changeCid + "|";

            //展示 商家id^车源id^是否是推荐车^是否已售^车类型^展现位置^千人千面推荐类型^竞价广告id^城市id|     

            _this.infoList += carinfo.did + "^" + carinfo.id + "^" + carinfo.installment + "^" + (carinfo.selled == 5 ? 1 : 0) + "^" + carType + "^" + (i + 1) + "^" + 0 + "^" + (typeof carinfo.adid == 'undefined' ? 0 : carinfo.adid) + "^" + _this.changeCid + "|";
            carlist.push("<a target=\"_blank\" onmousedown=\"eventkeyADDMore('" + (type == 2 ? 'c_pc_2sc_carlist_otherrecom' : 'c_pc_2sc_carlist_localrecom') + "','" + info + "')\" href=\"" +
               url + "\">");
            carlist.push("<img  src=\"" + carinfo.pic + "\" class=\"photo\">");
            carlist.push("<div class=\"list-photo-info\">");
            carlist.push("<h3>" + carinfo.carname + "</h3>");
            carlist.push("<div class=\"time\">" + carinfo.milage + "万公里／" + carinfo.date + "／" + carinfo.cityname + "</div>");
            carlist.push("<div class=\"price\"><em><b>" + carinfo.price + "</b>万元</em></div></div></a>");
            if (_this.getIconList(carinfo).length > 0) {
                carlist.push('<div class="tag-area">')
                carlist.push(_this.getIconList(carinfo));
                carlist.push('</div>');
            }
            carlist.push("</li>");
        }
        return carlist.join('');
    },
    this.getIconList = function (obj) {
        var strHtml = [];
        if (obj.vincodestatus == 2) {
            //维修保养记录已查
            _this.listIcon.push("维修保养记录已查");
        }
        if (obj.haswarranty == 1) {
            //质保
            strHtml.push("<a href=\"/help.html#3\" class=\"tag-quality\" title=\"原厂质保\"></a>");
        } else if (obj.haswarranty == 2 && (obj.linktype == 2 || obj.linktype == 3)) {
            //质保
            strHtml.push("<a href=\"/help.html#4\" class=\"tag-quality\" title=\"商家提供延长质保\"></a>");
        }
        if (obj.linktype == 22 || obj.linktype == 23) {
            //品牌认证
            strHtml.push("<a href=\"/help.html#2\" class=\"tag-brand\" title=\"品牌认证，享原厂延保\"></a>");
        }
        if (obj.isnewcar == 1) {
            //准新车
            strHtml.push("<a href=\"/help.html\" class=\"tag-new-almost\" title=\"1年且两万公里内准新车源\"></a>");
        }

        return strHtml.join('');
    }
}
var moreCar = new addMoreCar();
moreCar.loadCar();

/*列表页车源下拉加载模块,自动加载三页数据
pingyang@autohome.com.cn*/
var ViewListAutoLoad = function () {
    var islock = false;//异步锁,防止多次调用
    var isLoadPage1 = false;//第1页是否加载
    var isLoadPage2 = false;//第2页是否加载
    var IsABTest = "1";//是否应用ABTEST
    var pageindex = 1;//自动加载三页数据,从1开始
    var TotalPage = parseInt($("#TotalPage").val()); //总页数
    var solrfilterinfo = SolrFilterInfo.Convert($("#filterInfoJs").val());
    /*判断是否滚到指定位置*/
    var _inView = function (element) {
        var box = document.getElementById(element);// 获取元素 
        if (box) {
            var offset = -30;//
            var coords = box.getBoundingClientRect();
            return ((coords.top >= 0 && coords.left >= 0 && coords.top)
			  <= (window.innerHeight || document.documentElement.clientHeight) + offset);
        }
    };
    /*加载数据*/
    var _loadInfos = function () {
        islock = true;
        $("#loadmorelist").show();
        var _filterval = $("#filterInfo").val();
        var _CarOffset = $("#FilterInfoCarOffset").val();
        var _OccupationCarPlaceCnt = $("#FilterInfoOccupationCarPlaceCnt").val();
        var url = "/Handler/CarList/GetViewList.ashx?" + _filterval + '&curpageindex=' + pageindex + '&carOffset=' + _CarOffset + '&occupationCarPlaceCnt=' + _OccupationCarPlaceCnt + '&kw=' + $("#filterKw").val() + '&v=20161025141121547';
        $.ajax({
            url: url,
            success: function (data) {
                islock = false;
                if (data != 'NODATA') {
                    var datalist = data.split("$$");
                    $("#viewlist_ul").append(datalist[2]);
                    $("#listpagination").empty().append(datalist[0]);
                    var curref = solrfilterinfo.BuilderUrl();
                    solrfilterinfo.PageIdx = solrfilterinfo.PageIdx + 1;
                    var locationhref = solrfilterinfo.BuilderUrl();
                    $.ajax({
                        type: "GET",
                        url: "http://collection.pv.che168.com/collect/page_event.ashx?eventkey=s_pc_2sc_carlist_carshow&info=" + datalist[1] + "&ref=" + escape(curref) + "&cur=" + escape(locationhref) + "&rm=" + new Date().getTime(),
                        dataType: "jsonp"
                    });
                }
                if (pageindex == 1) {
                    isLoadPage1 = true;
                } else if (pageindex == 2) {
                    isLoadPage2 = true;
                }
                pageindex++;
                $("#loadmorelist").hide();
            }
        });

    };
    /*初始化,列表模式时才应用此规则.40%小流量实验
		20161021-pingyang@autohome
	*/
    this.Init = function () {
        IsABTest = $("#IsABTest_SyncLoadViewList").val();
        if (IsABTest == "1" && solrfilterinfo.ListView == 0 && TotalPage > 1 && solrfilterinfo.PageIdx < TotalPage) {
            var carlist_smallpv = $.getCookie("o_pc_2sc_carlist_smallpv", "0");
            if (carlist_smallpv == "0") {
                eventkeyADDMore("o_pc_2sc_carlist_smallpv", $.getCookie("sessionid"));
                $.setCookie("o_pc_2sc_carlist_smallpv", "1", { expireHours: 24 * 30 * 3 });
            }

            $(window).scroll(function () {
                if (!(isLoadPage1 && isLoadPage2)) {
                    if (!islock) {
                        if (_inView("listpagination")) {
                            _loadInfos();
                        }
                    }
                } else {
                    $("#loadmorelist").hide();
                }
            });
        } else {
            $("#loadmorelist").hide();
        }
    };

};
var viewListAutoLoad = new ViewListAutoLoad();
viewListAutoLoad.Init();

function eventkeyADDMore(obj, param) {
    $.ajax({
        type: "GET",
        url: "http://collection.pv.che168.com/collect/page_event.ashx?eventkey=" + obj + "&info=" + param + "&ref=" + escape(document.referrer) + "&cur=" + escape(location.href) + "&rm=" + new Date().getTime(),
        dataType: "jsonp"
    });
}

function page_eventkey(key, info, ref, cur) {
    $.ajax({
        type: "GET",
        url: "http://collection.pv.che168.com/collect/page_event.ashx?eventkey=" + key + "&info=" + info + "&ref=" + escape(ref) + "&cur=" + escape(cur) + "&rm=" + new Date().getTime(),
        dataType: "jsonp"
    });
}

/*
Execute Time:6ms
Create Time:2017/3/31 17:06:10
*/
